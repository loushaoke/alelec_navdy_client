package com.navdy.service.library.events.glances;

import com.squareup.wire.ProtoEnum;

public enum GlanceIconConstants implements ProtoEnum {
    GLANCE_ICON_NAVDY_MAIN(0),
    GLANCE_ICON_MESSAGE_SIDE_BLUE(1);
    
    private final int value;

    private GlanceIconConstants(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
