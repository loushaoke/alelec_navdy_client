package com.navdy.service.library.events.places;

import com.navdy.service.library.events.destination.Destination;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class DestinationSelectedRequest extends Message {
    public static final String DEFAULT_REQUEST_ID = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 2)
    public final Destination destination;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String request_id;

    public static final class Builder extends com.squareup.wire.Message.Builder<DestinationSelectedRequest> {
        public Destination destination;
        public String request_id;

        public Builder(DestinationSelectedRequest message) {
            super(message);
            if (message != null) {
                this.request_id = message.request_id;
                this.destination = message.destination;
            }
        }

        public Builder request_id(String request_id) {
            this.request_id = request_id;
            return this;
        }

        public Builder destination(Destination destination) {
            this.destination = destination;
            return this;
        }

        public DestinationSelectedRequest build() {
            return new DestinationSelectedRequest(this, null);
        }
    }

    public DestinationSelectedRequest(String request_id, Destination destination) {
        this.request_id = request_id;
        this.destination = destination;
    }

    private DestinationSelectedRequest(Builder builder) {
        this(builder.request_id, builder.destination);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof DestinationSelectedRequest)) {
            return false;
        }
        DestinationSelectedRequest o = (DestinationSelectedRequest) other;
        if (equals((Object) this.request_id, (Object) o.request_id) && equals((Object) this.destination, (Object) o.destination)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.request_id != null) {
            result = this.request_id.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.destination != null) {
            i = this.destination.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
