package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class VoiceAssistResponse extends Message {
    public static final VoiceAssistState DEFAULT_STATE = VoiceAssistState.VOICE_ASSIST_AVAILABLE;
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final VoiceAssistState state;

    public static final class Builder extends com.squareup.wire.Message.Builder<VoiceAssistResponse> {
        public VoiceAssistState state;

        public Builder(VoiceAssistResponse message) {
            super(message);
            if (message != null) {
                this.state = message.state;
            }
        }

        public Builder state(VoiceAssistState state) {
            this.state = state;
            return this;
        }

        public VoiceAssistResponse build() {
            checkRequiredFields();
            return new VoiceAssistResponse(this);
        }
    }

    public enum VoiceAssistState implements ProtoEnum {
        VOICE_ASSIST_AVAILABLE(0),
        VOICE_ASSIST_NOT_AVAILABLE(1),
        VOICE_ASSIST_LISTENING(2),
        VOICE_ASSIST_STOPPED(3);
        
        private final int value;

        private VoiceAssistState(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public VoiceAssistResponse(VoiceAssistState state) {
        this.state = state;
    }

    private VoiceAssistResponse(Builder builder) {
        this(builder.state);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof VoiceAssistResponse) {
            return equals((Object) this.state, (Object) ((VoiceAssistResponse) other).state);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.state != null ? this.state.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
