package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class NowPlayingUpdateRequest extends Message {
    public static final Boolean DEFAULT_START = Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.BOOL)
    public final Boolean start;

    public static final class Builder extends com.squareup.wire.Message.Builder<NowPlayingUpdateRequest> {
        public Boolean start;

        public Builder(NowPlayingUpdateRequest message) {
            super(message);
            if (message != null) {
                this.start = message.start;
            }
        }

        public Builder start(Boolean start) {
            this.start = start;
            return this;
        }

        public NowPlayingUpdateRequest build() {
            checkRequiredFields();
            return new NowPlayingUpdateRequest(this);
        }
    }

    public NowPlayingUpdateRequest(Boolean start) {
        this.start = start;
    }

    private NowPlayingUpdateRequest(Builder builder) {
        this(builder.start);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof NowPlayingUpdateRequest) {
            return equals((Object) this.start, (Object) ((NowPlayingUpdateRequest) other).start);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.start != null ? this.start.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
