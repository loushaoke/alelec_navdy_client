package com.navdy.service.library.network;

import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionType;
import java.io.Closeable;
import java.io.IOException;

public interface SocketAcceptor extends Closeable {
    SocketAdapter accept() throws IOException;

    void close() throws IOException;

    ConnectionInfo getRemoteConnectionInfo(SocketAdapter socketAdapter, ConnectionType connectionType);
}
