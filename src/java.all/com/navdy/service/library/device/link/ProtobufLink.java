package com.navdy.service.library.device.link;

import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.util.IOUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.LinkedBlockingDeque;

public class ProtobufLink implements Link {
    private int bandwidthLevel = 1;
    private ConnectionInfo connectionInfo;
    private ReaderThread readerThread;
    private WriterThread writerThread;

    public ProtobufLink(ConnectionInfo connectionInfo) {
        this.connectionInfo = connectionInfo;
    }

    public boolean start(SocketAdapter socket, LinkedBlockingDeque<EventRequest> outboundEventQueue, LinkListener listener) {
        if ((this.writerThread == null || this.writerThread.isClosing()) && (this.readerThread == null || this.readerThread.isClosing())) {
            InputStream inputStream = null;
            try {
                inputStream = socket.getInputStream();
                OutputStream outputStream = socket.getOutputStream();
                this.readerThread = new ReaderThread(this.connectionInfo.getType(), inputStream, listener, true);
                this.writerThread = new WriterThread(outboundEventQueue, outputStream, null);
                this.readerThread.start();
                this.writerThread.start();
                return true;
            } catch (IOException e) {
                IOUtils.closeStream(inputStream);
                this.readerThread = null;
                this.writerThread = null;
                return false;
            }
        }
        throw new IllegalStateException("Must stop threads before calling startLink");
    }

    public void setBandwidthLevel(int level) {
        this.bandwidthLevel = level;
    }

    public int getBandWidthLevel() {
        return this.bandwidthLevel;
    }

    public void close() {
        if (this.writerThread != null) {
            this.writerThread.cancel();
            this.writerThread = null;
        }
        if (this.readerThread != null) {
            this.readerThread.cancel();
            this.readerThread = null;
        }
    }
}
