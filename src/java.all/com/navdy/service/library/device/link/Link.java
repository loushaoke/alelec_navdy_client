package com.navdy.service.library.device.link;

import com.navdy.service.library.network.SocketAdapter;
import java.io.Closeable;
import java.util.concurrent.LinkedBlockingDeque;

public interface Link extends Closeable {
    public static final int BANDWIDTH_LEVEL_LOW = 0;
    public static final int BANDWIDTH_LEVEL_NORMAL = 1;

    void close();

    int getBandWidthLevel();

    void setBandwidthLevel(int i);

    boolean start(SocketAdapter socketAdapter, LinkedBlockingDeque<EventRequest> linkedBlockingDeque, LinkListener linkListener);
}
