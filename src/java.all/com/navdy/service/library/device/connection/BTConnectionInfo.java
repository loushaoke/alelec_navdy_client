package com.navdy.service.library.device.connection;

import com.google.gson.annotations.SerializedName;
import com.navdy.service.library.device.NavdyDeviceId;
import java.util.UUID;

public class BTConnectionInfo extends ConnectionInfo {
    @SerializedName("address")
    ServiceAddress mAddress;

    public BTConnectionInfo(NavdyDeviceId id, ServiceAddress address, ConnectionType connectionType) {
        super(id, connectionType);
        this.mAddress = address;
    }

    public BTConnectionInfo(NavdyDeviceId id, UUID serviceUUID, ConnectionType connectionType) {
        super(id, connectionType);
        if (id.getBluetoothAddress() != null) {
            this.mAddress = new ServiceAddress(id.getBluetoothAddress(), serviceUUID.toString());
            return;
        }
        throw new IllegalArgumentException("BT address missing");
    }

    public ServiceAddress getAddress() {
        return this.mAddress;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass() || !super.equals(o)) {
            return false;
        }
        return this.mAddress.equals(((BTConnectionInfo) o).mAddress);
    }

    public int hashCode() {
        return (super.hashCode() * 31) + this.mAddress.hashCode();
    }
}
