package com.navdy.service.library.device.connection.tunnel;

import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.NetworkActivityTracker;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

class TransferThread extends Thread {
    static final Logger sLogger = new Logger(TransferThread.class);
    final NetworkActivityTracker activityTracker = NetworkActivityTracker.getInstance();
    private volatile boolean canceled;
    final InputStream inStream;
    final OutputStream outStream;
    final Tunnel parentThread;
    final boolean sending;

    public TransferThread(Tunnel parentThread, InputStream inStream, OutputStream outStream, boolean sending) {
        sLogger.v("new transfer thread (" + this + "): " + inStream + " -> " + outStream);
        setName(TransferThread.class.getSimpleName());
        this.parentThread = parentThread;
        this.inStream = inStream;
        this.outStream = outStream;
        this.sending = sending;
        parentThread.transferThreads.add(this);
    }

    public void run() {
        byte[] buffer = new byte[16384];
        while (true) {
            try {
                int bytes = this.inStream.read(buffer);
                if (bytes < 0) {
                    sLogger.d("socket was closed");
                    break;
                }
                if (sLogger.isLoggable(2)) {
                    byte[] d = Arrays.copyOf(buffer, bytes);
                    sLogger.v(String.format("transfer (%s -> %s): %d bytes%s", new Object[]{this.inStream, this.outStream, Integer.valueOf(bytes), ""}));
                }
                this.outStream.write(buffer, 0, bytes);
                this.outStream.flush();
                if (this.sending) {
                    this.activityTracker.addBytesSent(bytes);
                } else {
                    this.activityTracker.addBytesReceived(bytes);
                }
            } catch (Throwable e) {
                if (!this.canceled) {
                    sLogger.e("Exception", e);
                }
            }
        }
        cancel();
    }

    public void cancel() {
        this.canceled = true;
        IOUtils.closeStream(this.inStream);
        IOUtils.closeStream(this.outStream);
        this.parentThread.transferThreads.remove(this);
    }
}
