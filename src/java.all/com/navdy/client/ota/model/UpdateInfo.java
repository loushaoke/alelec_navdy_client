package com.navdy.client.ota.model;

import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.service.library.log.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class UpdateInfo {
    private static final String DEFAULT_RELEASE_NOTES_LANGUAGE_TAG = "en";
    private static final String META_DATA_RELEASE_NOTES_TAG = "release_notes";
    public static final Logger sLogger = new Logger(UpdateInfo.class);
    public String description;
    public int fromVersion;
    public boolean incremental;
    public String metaData;
    public long size;
    public String url;
    public int version;
    public String versionName;

    public boolean equals(Object o) {
        if (!(o instanceof UpdateInfo)) {
            return super.equals(o);
        }
        UpdateInfo other = (UpdateInfo) o;
        return other.version == this.version && StringUtils.equalsOrBothEmptyAfterTrim(other.versionName, this.versionName) && other.incremental == this.incremental && other.fromVersion == this.fromVersion && StringUtils.equalsOrBothEmptyAfterTrim(other.url, this.url);
    }

    public int hashCode() {
        return 0;
    }

    public static String extractReleaseNotes(UpdateInfo updateInfo) {
        sLogger.d("Parsing meta data to get release notes");
        if (updateInfo == null) {
            sLogger.d("updateInfo is null");
            return null;
        }
        String metaData = updateInfo.metaData;
        if (metaData != null) {
            sLogger.d("Meta data node present");
            try {
                JSONObject releaseNotes = new JSONObject(metaData).getJSONObject("release_notes");
                if (releaseNotes != null) {
                    return releaseNotes.getString(DEFAULT_RELEASE_NOTES_LANGUAGE_TAG);
                }
                sLogger.d("Release notes node not found");
            } catch (JSONException e) {
                sLogger.e("Error parsing the JSON metadata ", e);
            }
        } else {
            sLogger.d("Meta data node not present");
        }
        return null;
    }

    public static String getFormattedVersionName(UpdateInfo updateInfo) {
        if (StringUtils.isEmptyAfterTrim(updateInfo.versionName)) {
            return String.valueOf(updateInfo.version);
        }
        return updateInfo.versionName.split("-")[0];
    }
}
