package com.navdy.client.app.framework;

import com.navdy.client.app.framework.servicehandler.NetworkStatusManager;
import com.navdy.service.library.util.Listenable;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class AppInstance$$InjectAdapter extends Binding<AppInstance> implements MembersInjector<AppInstance> {
    private Binding<NetworkStatusManager> mNetworkStatus;
    private Binding<Listenable> supertype;

    public AppInstance$$InjectAdapter() {
        super(null, "members/com.navdy.client.app.framework.AppInstance", false, AppInstance.class);
    }

    public void attach(Linker linker) {
        this.mNetworkStatus = linker.requestBinding("com.navdy.client.app.framework.servicehandler.NetworkStatusManager", AppInstance.class, getClass().getClassLoader());
        Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.service.library.util.Listenable", AppInstance.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mNetworkStatus);
        injectMembersBindings.add(this.supertype);
    }

    public void injectMembers(AppInstance object) {
        object.mNetworkStatus = (NetworkStatusManager) this.mNetworkStatus.get();
        this.supertype.injectMembers(object);
    }
}
