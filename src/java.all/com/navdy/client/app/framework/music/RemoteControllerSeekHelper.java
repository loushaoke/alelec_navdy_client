package com.navdy.client.app.framework.music;

import android.media.RemoteController;
import android.os.Handler;
import com.navdy.client.app.framework.servicehandler.MusicServiceHandler;
import com.navdy.client.app.framework.servicehandler.MusicServiceHandler.MusicSeekHelper;
import com.navdy.service.library.events.audio.MusicEvent.Action;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.log.Logger;

public class RemoteControllerSeekHelper implements MusicSeekHelper, Runnable {
    private static final Handler handler = new Handler();
    private final int CONSECUTIVE_BREAK = 300;
    private final int STEP = 10000;
    private final Logger logger = new Logger(RemoteControllerSeekHelper.class);
    private final RemoteController remoteController;
    private int signedStep;

    public RemoteControllerSeekHelper(RemoteController remoteController) {
        this.remoteController = remoteController;
    }

    public void executeMusicSeekAction(Action action) {
        this.logger.v("Executing music action " + String.valueOf(action));
        if (Action.MUSIC_ACTION_FAST_FORWARD_STOP.equals(action) || Action.MUSIC_ACTION_REWIND_STOP.equals(action)) {
            handler.removeCallbacks(this);
            return;
        }
        this.signedStep = Action.MUSIC_ACTION_FAST_FORWARD_START.equals(action) ? 10000 : -10000;
        handler.post(this);
    }

    public void run() {
        int oldPosition = (int) this.remoteController.getEstimatedMediaPosition();
        if (oldPosition < 0) {
            this.logger.i("Music position unavailable, can't seek");
            return;
        }
        int newPosition = oldPosition + this.signedStep;
        MusicServiceHandler musicServiceHandler = MusicServiceHandler.getInstance();
        if (musicServiceHandler.isMusicPlayerActive()) {
            MusicTrackInfo currentTrack = musicServiceHandler.getCurrentMediaTrackInfo();
            if (newPosition < 0) {
                newPosition = 0;
            } else if (currentTrack.duration != null && currentTrack.duration.intValue() > 0 && newPosition > currentTrack.duration.intValue()) {
                newPosition = currentTrack.duration.intValue();
            }
            this.logger.v("Setting new position: " + oldPosition + " -> " + newPosition);
            this.remoteController.seekTo((long) newPosition);
            handler.postDelayed(this, 300);
            return;
        }
        this.logger.e("Tried to seek with no current track");
    }
}
