package com.navdy.client.app.framework.util;

import android.database.ContentObserver;
import android.os.Handler;
import com.navdy.client.app.ui.homescreen.CalendarUtils;
import com.navdy.service.library.log.Logger;
import java.util.concurrent.atomic.AtomicBoolean;

public class CalendarObserver extends ContentObserver {
    private static final int DELAY_THRESHOLD = 10000;
    private static final boolean VERBOSE = false;
    private static final Logger logger = new Logger(CalendarObserver.class);
    private Handler handler;
    private long lastSendTime = 0;
    private Runnable sendRunnable = new Runnable() {
        public void run() {
            CalendarObserver.logger.i("should send calendar events in runnable: " + CalendarObserver.this.shouldSendToHud);
            if (CalendarObserver.this.shouldSendToHud.getAndSet(false)) {
                CalendarObserver.this.lastSendTime = System.currentTimeMillis();
                CalendarUtils.sendCalendarsToHud();
                CalendarObserver.this.postSendCalendarRunnable();
            }
        }
    };
    private AtomicBoolean shouldSendToHud = new AtomicBoolean(false);

    public CalendarObserver(Handler handler) {
        super(handler);
        this.handler = handler;
    }

    public void postSendCalendarRunnable() {
        this.handler.removeCallbacks(this.sendRunnable);
        this.handler.postDelayed(this.sendRunnable, 10000);
    }

    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        synchronized (this) {
            this.shouldSendToHud.set(true);
            if (System.currentTimeMillis() >= this.lastSendTime + 10000) {
                this.sendRunnable.run();
            }
        }
    }
}
