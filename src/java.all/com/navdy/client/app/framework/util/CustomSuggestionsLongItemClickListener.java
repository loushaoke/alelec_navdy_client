package com.navdy.client.app.framework.util;

import android.view.View;
import com.navdy.client.app.framework.models.Suggestion;

public interface CustomSuggestionsLongItemClickListener {
    boolean onItemLongClick(View view, int i, Suggestion suggestion);
}
