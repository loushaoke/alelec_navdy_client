package com.navdy.client.app.framework.util;

import android.view.View;

public interface CustomItemClickListener {
    void onClick(View view, int i);
}
