package com.navdy.client.app.framework.util;

import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.text.Html;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.TextUtils;
import com.navdy.client.app.NavdyApplication;
import java.util.Iterator;
import java.util.regex.Pattern;

public class StringUtils {
    public static final String GOOGLE_MAPS_REGEXP = "cacheResponse\\(\\[\\[\\[(-?[0-9]+(\\.[0-9]+)\\,){2}(-?[0-9]+(\\.[0-9]+)){1}\\]";
    public static final String LAT_LNG_REGEXP = "(-?[0-9]+\\.?[0-9]*), *(-?[0-9]+\\.?[0-9]*)";
    public static final String NOT_WHITESPACE_REGEXP = "[^\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000]";
    private static final String WHITESPACE_CHARS = "\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000";
    public static final String WHITESPACE_REGEXP = "[\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000]";

    public static boolean equalsOrBothEmptyAfterTrim(CharSequence s1, CharSequence s2) {
        return (isEmptyAfterTrim(s1) && isEmptyAfterTrim(s2)) || !(isEmptyAfterTrim(s1) || isEmptyAfterTrim(s2) || !TextUtils.equals(s1.toString().trim(), s2.toString().trim()));
    }

    public static boolean isEmptyAfterTrim(CharSequence s) {
        return s == null || s.toString().trim().length() <= 0;
    }

    public static long countOccurrencesOf(String s, char c) {
        if (s == null) {
            return 0;
        }
        long num = 0;
        for (int i = 0; i < s.length(); i++) {
            num += (long) (s.charAt(i) == c ? 1 : 0);
        }
        return num;
    }

    public static String join(Iterable<?> iterable, String separator) {
        StringBuilder sb = new StringBuilder();
        if (iterable != null) {
            Iterator<?> iterator = iterable.iterator();
            while (iterator.hasNext()) {
                sb.append(iterator.next().toString());
                if (iterator.hasNext()) {
                    sb.append(separator);
                }
            }
        }
        return sb.toString();
    }

    public static boolean isValidText(CharSequence text) {
        return !isEmptyAfterTrim(text) && TextUtils.getTrimmedLength(text) > 0;
    }

    public static boolean isValidName(CharSequence nameText) {
        if (isValidText(nameText)) {
            return Pattern.compile(".*\\p{L}+.*").matcher(nameText).matches();
        }
        return false;
    }

    public static boolean isValidEmail(CharSequence emailText) {
        if (isValidText(emailText)) {
            return Pattern.compile("^([A-Za-z0-9.=!#$%&'*+\\-_~/^`|\\{}]+)@((\\[(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))\\])|(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))|([A-Za-z0-9\\-.]+\\.[A-Za-z\\-]+))$").matcher(emailText).matches();
        }
        return false;
    }

    public String[] concatStringArrays(String[] a, String[] b) {
        int aLen = a.length;
        int bLen = b.length;
        String[] c = new String[(aLen + bLen)];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }

    public static Spanned fromHtml(@StringRes int stringId) {
        return fromHtml(NavdyApplication.getAppContext().getString(stringId));
    }

    public static Spanned fromHtml(@StringRes int stringId, Object... param) {
        return fromHtml(NavdyApplication.getAppContext().getString(stringId, param));
    }

    public static Spanned fromHtml(String string) {
        if (string == null) {
            return null;
        }
        if (isEmptyAfterTrim(string)) {
            return new SpannedString(string);
        }
        if (VERSION.SDK_INT >= 24) {
            return Html.fromHtml(string, 0);
        }
        return Html.fromHtml(string);
    }

    @NonNull
    public static String replaceSafely(@NonNull String line, @Nullable String lookFor, @Nullable String replaceWith) {
        return (lookFor == null || replaceWith == null) ? line : line.replace(lookFor, replaceWith);
    }

    @NonNull
    public static String replaceOrErase(@NonNull String line, @Nullable String lookFor, @Nullable String replaceWith) {
        if (lookFor == null) {
            return line;
        }
        if (replaceWith == null) {
            replaceWith = "";
        }
        String replaced = line.replace(lookFor, replaceWith);
        return !isEmptyAfterTrim(replaced) ? replaced.trim() : replaced;
    }

    @NonNull
    public static String replaceAllSafely(@NonNull String line, @Nullable String lookFor, @Nullable String replaceWith) {
        return (lookFor == null || replaceWith == null) ? line : line.replaceAll(lookFor, replaceWith);
    }

    public static boolean containsAfterTrim(@Nullable String source, @Nullable String toBeFound) {
        if (isEmptyAfterTrim(source) || isEmptyAfterTrim(toBeFound)) {
            return false;
        }
        return source.contains(toBeFound);
    }
}
