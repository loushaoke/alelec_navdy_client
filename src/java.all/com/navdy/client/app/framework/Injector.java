package com.navdy.client.app.framework;

import android.content.Context;
import dagger.ObjectGraph;

public final class Injector {
    private static final String INJECTOR_SERVICE = "com.navdy.app.client.injector";

    public static ObjectGraph obtain(Context context) {
        return (ObjectGraph) context.getSystemService(INJECTOR_SERVICE);
    }

    public static void inject(Context context, Object object) {
        ObjectGraph objectGraph = obtain(context);
        if (objectGraph != null) {
            objectGraph.inject(object);
            return;
        }
        throw new IllegalArgumentException("Please provide application context");
    }

    public static boolean matchesService(String name) {
        return INJECTOR_SERVICE.equals(name);
    }
}
