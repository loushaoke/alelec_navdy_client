package com.navdy.client.app.framework;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build.VERSION;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.ui.firstlaunch.ScreenLightActivity;
import com.navdy.service.library.log.Logger;

public class FlashlightManager {
    private static FlashlightManager instance = null;
    private Camera cam;
    private boolean flashlightIsOn = false;
    private boolean isCapableOfFlashlighting = false;
    private Logger logger = new Logger(FlashlightManager.class);
    private float previousScreenBrightness = 0.5f;

    private FlashlightManager() {
        PackageManager packageManager = NavdyApplication.getAppContext().getPackageManager();
        if (packageManager != null) {
            this.isCapableOfFlashlighting = packageManager.hasSystemFeature("android.hardware.camera.flash");
        }
    }

    public static FlashlightManager getInstance() {
        if (instance == null) {
            instance = new FlashlightManager();
        }
        return instance;
    }

    public void switchLight(Activity activity) {
        if (this.flashlightIsOn) {
            turnFlashLightOff();
        } else {
            turnFlashLightOn(activity);
        }
    }

    public void turnFlashLightOn(Activity activity) {
        try {
            if (!this.flashlightIsOn) {
                if (this.isCapableOfFlashlighting) {
                    if (VERSION.SDK_INT >= 23) {
                        setTorchModeOn(true);
                    } else {
                        this.cam = Camera.open();
                        Parameters p = this.cam.getParameters();
                        p.setFlashMode("torch");
                        this.cam.setParameters(p);
                        this.cam.startPreview();
                    }
                    this.flashlightIsOn = true;
                    return;
                }
                throw new Exception("Phone not capable of flashlighting");
            }
        } catch (Exception e) {
            this.isCapableOfFlashlighting = false;
            this.logger.e("Unable to turn the flashlight on the normal way. Using the screen brightness instead. " + e.getMessage());
            startScreenLightActivity(activity);
        }
    }

    public void turnFlashLightOff() {
        try {
            if (!this.flashlightIsOn) {
                return;
            }
            if (this.isCapableOfFlashlighting) {
                if (VERSION.SDK_INT >= 23) {
                    setTorchModeOn(false);
                } else {
                    this.cam.stopPreview();
                    this.cam.release();
                    this.cam = null;
                }
                this.flashlightIsOn = false;
                return;
            }
            throw new Exception("Phone not capable of flashlighting");
        } catch (Exception e) {
            this.logger.e("Unable to turn the flashlight off the normal way. " + e.getMessage());
        }
    }

    @TargetApi(23)
    public void setTorchModeOn(boolean isOn) throws CameraAccessException {
        CameraManager manager = (CameraManager) NavdyApplication.getAppContext().getSystemService("camera");
        if (manager != null) {
            String[] cameraList = manager.getCameraIdList();
            if (cameraList.length > 0) {
                manager.setTorchMode(cameraList[0], isOn);
            }
        }
    }

    public void startScreenLightActivity(Activity activity) {
        activity.startActivity(new Intent(activity.getApplicationContext(), ScreenLightActivity.class));
    }

    public void setScreenToFullBrightness(Window window) {
        LayoutParams layout = window.getAttributes();
        this.previousScreenBrightness = layout.screenBrightness;
        layout.screenBrightness = 1.0f;
        window.setAttributes(layout);
    }

    public void setScreenBackToNormalBrightness(Window window) {
        LayoutParams layout = window.getAttributes();
        layout.screenBrightness = this.previousScreenBrightness;
        window.setAttributes(layout);
    }
}
