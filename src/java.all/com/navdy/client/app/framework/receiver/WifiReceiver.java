package com.navdy.client.app.framework.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.suggestion.DestinationSuggestionService;
import com.navdy.client.app.framework.util.SupportTicketService;
import com.navdy.client.app.service.DataCollectionService;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.ota.OTAUpdateService;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.SystemUtils;

public class WifiReceiver extends BroadcastReceiver {
    private static final Logger logger = new Logger(WifiReceiver.class);

    public void onReceive(Context context, Intent intent) {
        logger.v("onReceive connection");
        Context appContext = NavdyApplication.getAppContext();
        if (SystemUtils.isConnectedToWifi(appContext) || !SettingsUtils.isLimitingCellularData()) {
            if (SystemUtils.isConnectedToNetwork(appContext)) {
                SupportTicketService.submitTicketsIfConditionsAreMet();
            }
            if (SystemUtils.isConnectedToWifi(appContext)) {
                logger.d("Wi-Fi connected. Starting OTA Service");
                Intent i = OTAUpdateService.getServiceIntent(appContext);
                i.putExtra(OTAUpdateService.EXTRA_WIFI_TRIGGER, true);
                appContext.startService(i);
                DestinationSuggestionService.uploadTripDatabase(context, false);
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        DataCollectionService.sendToS3IfNotAlreadySending();
                    }
                }, 1);
                return;
            }
            return;
        }
        logger.v("app is limiting cellular data, do not submit support tickets when wifi is off");
    }
}
