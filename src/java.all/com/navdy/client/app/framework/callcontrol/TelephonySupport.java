package com.navdy.client.app.framework.callcontrol;

import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import com.navdy.client.app.NavdyApplication;
import com.navdy.service.library.log.Logger;
import java.lang.reflect.Method;

public class TelephonySupport implements TelephonyInterface {
    private static final Logger sLogger = new Logger(TelephonySupport.class);
    private Context context;
    private TelephonyManager telephonyManager;

    public TelephonySupport(Context context) {
        this.context = context;
        this.telephonyManager = (TelephonyManager) context.getSystemService("phone");
    }

    public void acceptRingingCall() {
        Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
        intent.putExtra("android.intent.extra.KEY_EVENT", new KeyEvent(0, 79));
        this.context.sendOrderedBroadcast(intent, null);
        Intent intent2 = new Intent("android.intent.action.MEDIA_BUTTON");
        intent2.putExtra("android.intent.extra.KEY_EVENT", new KeyEvent(1, 79));
        this.context.sendOrderedBroadcast(intent2, null);
    }

    public void rejectRingingCall() {
        endCall();
    }

    public void endCall() {
        try {
            endPhoneCall();
        } catch (Throwable ex) {
            sLogger.d("endCall failed -" + ex.toString(), ex);
        }
    }

    public void toggleMute() {
        try {
            Method methodGetITelephony = Class.forName(this.telephonyManager.getClass().getName()).getDeclaredMethod("getITelephony", new Class[0]);
            methodGetITelephony.setAccessible(true);
            Object telephonyInterface = methodGetITelephony.invoke(this.telephonyManager, new Object[0]);
            Class telephonyInterfaceClass = Class.forName(telephonyInterface.getClass().getName());
            Method methodCallState = telephonyInterfaceClass.getDeclaredMethod("getCallState", new Class[0]);
            methodCallState.setAccessible(true);
            Method methodMute = telephonyInterfaceClass.getDeclaredMethod("mute", new Class[]{Boolean.TYPE});
            methodMute.setAccessible(true);
            if (((Integer) methodCallState.invoke(telephonyInterface, new Object[0])).intValue() == 2) {
                methodMute.invoke(telephonyInterface, new Object[]{Boolean.valueOf(true)});
                return;
            }
            methodMute.invoke(telephonyInterface, new Object[]{Boolean.valueOf(false)});
        } catch (Exception ex) {
            sLogger.e("toggleMute failed", ex);
        }
    }

    public static void endPhoneCall() throws Exception {
        TelephonyManager telephonyManager = (TelephonyManager) NavdyApplication.getAppContext().getSystemService("phone");
        Method methodGetITelephony = Class.forName(telephonyManager.getClass().getName()).getDeclaredMethod("getITelephony", new Class[0]);
        methodGetITelephony.setAccessible(true);
        Object telephonyInterface = methodGetITelephony.invoke(telephonyManager, new Object[0]);
        Class.forName(telephonyInterface.getClass().getName()).getDeclaredMethod("endCall", new Class[0]).invoke(telephonyInterface, new Object[0]);
    }
}
