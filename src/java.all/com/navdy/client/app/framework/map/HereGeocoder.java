package com.navdy.client.app.framework.map;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.OnEngineInitListener.Error;
import com.here.android.mpa.search.Address;
import com.here.android.mpa.search.Category;
import com.here.android.mpa.search.DiscoveryResultPage;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.Location;
import com.here.android.mpa.search.NavigationPosition;
import com.here.android.mpa.search.Place;
import com.here.android.mpa.search.PlaceLink;
import com.here.android.mpa.search.PlaceRequest;
import com.here.android.mpa.search.ResultListener;
import com.here.android.mpa.search.SearchRequest;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.models.Destination.Precision;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Event.Debug;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.log.Logger;
import java.util.List;
import org.droidparts.contract.SQL.DDL;

public final class HereGeocoder {
    private static final int MAX_COLLECTION_SIZE = 1;
    private static final String ROAD_ACCESS_TYPE = "road";
    private static final Handler handler = new Handler(Looper.getMainLooper());
    private static final Logger logger = new Logger(HereGeocoder.class);

    public interface OnGeocodeCompleteCallback {
        void onComplete(@NonNull GeoCoordinate geoCoordinate, @Nullable GeoCoordinate geoCoordinate2, @Nullable Address address, Precision precision);

        void onError(@NonNull ErrorCode errorCode);
    }

    private static class TimeoutWrappedResultListener<T> implements ResultListener<T> {
        private static final long GEOCODE_REQUEST_TIMEOUT_MILLIS = 30000;
        boolean cancelled;
        public boolean complete;
        private final ResultListener<T> listener;
        private final Runnable timeout = new Runnable() {
            public void run() {
                synchronized (TimeoutWrappedResultListener.this) {
                    if (TimeoutWrappedResultListener.this.complete) {
                        HereGeocoder.logger.v("Geocode call already complete");
                    } else {
                        TimeoutWrappedResultListener.this.cancelled = true;
                        TimeoutWrappedResultListener.this.listener.onCompleted(null, ErrorCode.CANCELLED);
                        Tracker.tagEvent(Debug.HERE_GEOCODE_TIMEOUT);
                        HereGeocoder.logger.w("Geocode call timed out");
                    }
                }
            }
        };

        TimeoutWrappedResultListener(ResultListener<T> listener) {
            this.listener = listener;
            HereGeocoder.handler.postDelayed(this.timeout, 30000);
        }

        public void onCompleted(T locations, ErrorCode errorCode) {
            synchronized (this) {
                HereGeocoder.handler.removeCallbacks(this.timeout);
                this.complete = true;
                if (this.cancelled) {
                    HereGeocoder.logger.e("Geocode call completed after timeout, error: " + errorCode);
                    Tracker.tagEvent(Debug.HERE_GEOCODE_COMPLETED_AFTER_TIMEOUT);
                } else {
                    HereGeocoder.logger.v("Geocode call completed successfully");
                    this.listener.onCompleted(locations, errorCode);
                }
            }
        }
    }

    private HereGeocoder() {
    }

    public static void makeRequest(final String searchQuery, final OnGeocodeCompleteCallback requestCallback) {
        if (!AppInstance.getInstance().canReachInternet()) {
            requestCallback.onError(ErrorCode.NETWORK_COMMUNICATION);
        }
        HereMapsManager.getInstance().addOnInitializedListener(new OnEngineInitListener() {
            public void onEngineInitializationCompleted(Error error) {
                if (error != Error.NONE) {
                    HereGeocoder.logger.e("makeHereRequestForDestination, OnEngineInitListener error: " + error);
                    requestCallback.onError(ErrorCode.NOT_INITIALIZED);
                } else if (searchQuery == null) {
                    HereGeocoder.logger.w("makeHereRequestForDestination, no searchQuery");
                    requestCallback.onError(ErrorCode.BAD_REQUEST);
                } else {
                    HereGeocoder.makeRequestInternal(NavdyLocationManager.getInstance().getSmartStartCoordinates(), searchQuery, requestCallback);
                }
            }
        });
    }

    private static void makeRequestInternal(Coordinate coordinate, String searchQuery, OnGeocodeCompleteCallback requestCallback) {
        if (requestCallback == null) {
            logger.w("makeHereRequestForDestination, no callback. Exiting");
        } else if (coordinate == null) {
            logger.w("makeHereRequestForDestination, no coordinates. Calling onError");
            requestCallback.onError(ErrorCode.BAD_LOCATION);
        } else {
            logger.v("makeHereRequestForDestination, searchQuery[" + searchQuery + "]" + " coordinate[" + coordinate.latitude + DDL.SEPARATOR + coordinate.longitude + "]");
            ErrorCode searchRequestError = new SearchRequest(searchQuery).setSearchCenter(new GeoCoordinate(coordinate.latitude.doubleValue(), coordinate.longitude.doubleValue())).setCollectionSize(1).execute(getSearchResultsListener(requestCallback));
            if (searchRequestError != ErrorCode.NONE) {
                requestCallback.onError(searchRequestError);
            }
        }
    }

    private static ResultListener<DiscoveryResultPage> getSearchResultsListener(final OnGeocodeCompleteCallback requestCallback) {
        return new TimeoutWrappedResultListener(new ResultListener<DiscoveryResultPage>() {
            public void onCompleted(DiscoveryResultPage discoveryResultPage, ErrorCode searchResponseError) {
                if (searchResponseError != ErrorCode.NONE) {
                    requestCallback.onError(searchResponseError);
                    return;
                }
                if (discoveryResultPage != null) {
                    List<PlaceLink> placeLinks = discoveryResultPage.getPlaceLinks();
                    if (placeLinks != null && placeLinks.size() > 0) {
                        PlaceLink placeLink = (PlaceLink) placeLinks.get(0);
                        if (placeLink != null) {
                            PlaceRequest detailsRequest = placeLink.getDetailsRequest();
                            if (detailsRequest != null) {
                                if (detailsRequest.execute(HereGeocoder.getPlaceResultListener(requestCallback)) != ErrorCode.NONE) {
                                    requestCallback.onError(searchResponseError);
                                    return;
                                }
                                return;
                            }
                        }
                    }
                }
                requestCallback.onError(searchResponseError);
            }
        });
    }

    @NonNull
    private static ResultListener<Place> getPlaceResultListener(final OnGeocodeCompleteCallback requestCallback) {
        return new TimeoutWrappedResultListener(new ResultListener<Place>() {
            public void onCompleted(Place place, ErrorCode placeResponseError) {
                if (placeResponseError != ErrorCode.NONE) {
                    requestCallback.onError(placeResponseError);
                    return;
                }
                if (place != null) {
                    Location location = place.getLocation();
                    if (location != null) {
                        GeoCoordinate displayCoord = location.getCoordinate();
                        GeoCoordinate navCoord = null;
                        List<NavigationPosition> accessPoints = location.getAccessPoints();
                        if (accessPoints != null) {
                            for (NavigationPosition accessPoint : accessPoints) {
                                if (accessPoint.getAccessType() != null && accessPoint.getAccessType().equals(HereGeocoder.ROAD_ACCESS_TYPE)) {
                                    navCoord = accessPoint.getCoordinate();
                                }
                            }
                        }
                        Precision precision = Precision.PRECISE;
                        List<Category> categories = place.getCategories();
                        if (categories != null) {
                            for (Category category : categories) {
                                String id = category.getId();
                                if (!StringUtils.equalsOrBothEmptyAfterTrim(id, "administrative-region") && !StringUtils.equalsOrBothEmptyAfterTrim(id, "city-town-village")) {
                                    if (StringUtils.equalsOrBothEmptyAfterTrim(id, "postal-area")) {
                                        precision = Precision.IMPRECISE;
                                        break;
                                    }
                                }
                                precision = Precision.IMPRECISE;
                                break;
                            }
                        }
                        if (displayCoord != null) {
                            requestCallback.onComplete(displayCoord, navCoord, location.getAddress(), precision);
                            return;
                        } else {
                            requestCallback.onError(placeResponseError);
                            return;
                        }
                    }
                }
                requestCallback.onError(placeResponseError);
            }
        });
    }
}
