package com.navdy.client.app.framework.service;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.RemoteController;
import android.media.RemoteController.MetadataEditor;
import android.media.RemoteController.OnClientUpdateListener;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.glances.GlanceConstants;
import com.navdy.client.app.framework.music.RemoteControllerSeekHelper;
import com.navdy.client.app.framework.servicehandler.MusicServiceHandler;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.service.library.events.audio.MusicCollectionSource;
import com.navdy.service.library.events.audio.MusicCollectionType;
import com.navdy.service.library.events.audio.MusicDataSource;
import com.navdy.service.library.events.audio.MusicPlaybackState;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.events.audio.MusicTrackInfo.Builder;
import com.navdy.service.library.log.Logger;
import org.droidparts.contract.SQL.DDL;

public class RemoteControllerNotificationListenerService extends NavdyCustomNotificationListenerService implements OnClientUpdateListener {
    private static final int MINIMUM_UPDATE_WAIT = 1000;
    private static final int REMOTE_CONTROL_PLAYSTATE_NONE = 0;
    private static final boolean VERBOSE = false;
    public static final Logger logger = new Logger(RemoteControllerNotificationListenerService.class);
    private Bitmap albumArt = null;
    private AudioManager audioManager;
    private boolean ignoreMetadata;
    private double lastUpdateTime;
    private boolean lastUpdateWasClientMetadata;
    @NonNull
    private MusicTrackInfo musicTrackInfo = MusicServiceHandler.EMPTY_TRACK_INFO;
    private int playbackState = 0;
    private RemoteController remoteController;

    public void onCreate() {
        logger.i("onCreate");
        super.onCreate();
    }

    private void initializeRemoteController() {
        logger.i("initializeRemoteController");
        if (this.remoteController != null) {
            logger.i("RemoteController already initialized, returning");
        } else {
            new Handler(getMainLooper()).post(new Runnable() {
                public void run() {
                    RemoteControllerNotificationListenerService.this.remoteController = new RemoteController(RemoteControllerNotificationListenerService.this, RemoteControllerNotificationListenerService.this);
                    if (!RemoteControllerNotificationListenerService.this.remoteController.setArtworkConfiguration(200, 200)) {
                        RemoteControllerNotificationListenerService.logger.e("Couldn't set artwork configuration");
                    }
                    RemoteControllerNotificationListenerService.this.audioManager = (AudioManager) NavdyApplication.getAppContext().getSystemService("audio");
                    try {
                        RemoteControllerNotificationListenerService.this.audioManager.registerRemoteController(RemoteControllerNotificationListenerService.this.remoteController);
                        MusicServiceHandler.getInstance().setMusicSeekHelper(new RemoteControllerSeekHelper(RemoteControllerNotificationListenerService.this.remoteController));
                        RemoteControllerNotificationListenerService.logger.i("Remote controller and audio manager initialized");
                    } catch (Throwable e) {
                        RemoteControllerNotificationListenerService.this.remoteController = null;
                        RemoteControllerNotificationListenerService.logger.e("Remote controller could not be initialized: " + e);
                    }
                }
            });
        }
    }

    public IBinder onBind(Intent intent) {
        logger.d("onBind");
        if (VERSION.SDK_INT < 21) {
            initializeRemoteController();
        }
        return super.onBind(intent);
    }

    public void onListenerConnected() {
        logger.d("onListenerConnected");
        initializeRemoteController();
        super.onListenerConnected();
    }

    public boolean onUnbind(Intent intent) {
        logger.d("onUnbind");
        return super.onUnbind(intent);
    }

    public void onDestroy() {
        logger.i("onDestroy");
        if (this.audioManager != null) {
            this.audioManager.unregisterRemoteController(this.remoteController);
        }
        MusicServiceHandler.getInstance().setMusicSeekHelper(null);
        super.onDestroy();
    }

    public void onClientChange(boolean clearing) {
        logger.v("onClientChange: " + clearing);
        if (clearing) {
            setMusicTrackInfo(null);
            this.albumArt = null;
            this.playbackState = 0;
        }
    }

    public void onClientPlaybackStateUpdate(int state) {
        logger.v("onClientPlaybackStateUpdate: " + state);
        checkForSpuriousStateUpdate(state);
        if (state != this.playbackState && state != 0) {
            this.playbackState = state;
            setMusicTrackInfo(new Builder(this.musicTrackInfo).playbackState(musicPlaybackState(this.playbackState)).collectionSource(MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN).collectionType(MusicCollectionType.COLLECTION_TYPE_UNKNOWN).collectionId(null).trackId(null).build());
            sendAlbumArt();
        }
    }

    public void onClientPlaybackStateUpdate(int state, long stateChangeTimeMs, long currentPosMs, float speed) {
        logger.v("onClientPlaybackStateUpdate: " + state + DDL.SEPARATOR + stateChangeTimeMs + DDL.SEPARATOR + currentPosMs + DDL.SEPARATOR + speed);
        checkForSpuriousStateUpdate(state);
        long currentTime = System.currentTimeMillis();
        if (state == 0) {
            return;
        }
        if (this.playbackState != state || ((double) currentTime) - this.lastUpdateTime >= 1000.0d || this.lastUpdateWasClientMetadata) {
            this.lastUpdateTime = (double) currentTime;
            this.lastUpdateWasClientMetadata = false;
            setMusicTrackInfo(new Builder(this.musicTrackInfo).playbackState(musicPlaybackState(state)).currentPosition(Integer.valueOf((int) currentPosMs)).collectionSource(MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN).collectionType(MusicCollectionType.COLLECTION_TYPE_UNKNOWN).collectionId(null).trackId(null).build());
            if (this.playbackState != state) {
                sendAlbumArt();
            }
            this.playbackState = state;
        }
    }

    private void checkForSpuriousStateUpdate(int state) {
        if (state >= 3 || this.playbackState >= 3) {
            this.ignoreMetadata = false;
            return;
        }
        logger.i("Spurious none/stop/pause detected, will ignore metadata updates until we leave none/stopped/paused states");
        this.ignoreMetadata = true;
    }

    public void onClientTransportControlUpdate(int transportControlFlags) {
        boolean z = true;
        logger.v("onClientTransportControlUpdate: " + Integer.toBinaryString(transportControlFlags));
        MusicServiceHandler musicServiceHandler = MusicServiceHandler.getInstance();
        Builder isPreviousAllowed = new Builder(this.musicTrackInfo).isPreviousAllowed(Boolean.valueOf((transportControlFlags & 1) != 0));
        if ((isAdvertisement() || !GlanceConstants.PANDORA.equals(musicServiceHandler.getLastMusicApp())) && (transportControlFlags & 128) == 0) {
            z = false;
        }
        setMusicTrackInfo(isPreviousAllowed.isNextAllowed(Boolean.valueOf(z)).collectionSource(MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN).collectionType(MusicCollectionType.COLLECTION_TYPE_UNKNOWN).collectionId(null).trackId(null).build());
        sendAlbumArt();
    }

    public void onClientMetadataUpdate(MetadataEditor metadataEditor) {
        logger.v("onClientMetadataUpdate");
        this.lastUpdateWasClientMetadata = true;
        if (this.ignoreMetadata) {
            logger.i("ignoring metadata due to prior spurious update");
            return;
        }
        setMusicTrackInfo(new Builder(this.musicTrackInfo).dataSource(MusicDataSource.MUSIC_SOURCE_OS_NOTIFICATION).album(metadataEditor.getString(1, "")).author(metadataEditor.getString(2, "")).name(metadataEditor.getString(7, "")).duration(Integer.valueOf((int) metadataEditor.getLong(9, 0))).playbackState(musicPlaybackState(this.playbackState)).collectionSource(MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN).collectionType(MusicCollectionType.COLLECTION_TYPE_UNKNOWN).collectionId(null).trackId(null).build());
        this.albumArt = null;
        Bitmap bitmap = metadataEditor.getBitmap(100, null);
        if (bitmap != null) {
            this.albumArt = bitmap;
            sendAlbumArt();
        }
    }

    public void onClientSessionEvent(String s, Bundle b) {
        logger.v("onClientSessionEvent");
    }

    private void setMusicTrackInfo(@Nullable MusicTrackInfo trackInfo) {
        if (trackInfo == null) {
            this.musicTrackInfo = MusicServiceHandler.EMPTY_TRACK_INFO;
        } else {
            this.musicTrackInfo = trackInfo;
        }
        MusicServiceHandler.getInstance().setCurrentMediaTrackInfo(trackInfo);
    }

    private void sendAlbumArt() {
        MusicServiceHandler.getInstance().checkAndSetCurrentMediaArtwork(this.musicTrackInfo, this.albumArt);
    }

    private boolean isAdvertisement() {
        MusicTrackInfo currentTrackInfo = MusicServiceHandler.getInstance().getCurrentMediaTrackInfo();
        String name = currentTrackInfo.name;
        return name != null && name.toLowerCase().contains("advertisement") && StringUtils.isEmptyAfterTrim(currentTrackInfo.author) && StringUtils.isEmptyAfterTrim(currentTrackInfo.album);
    }

    private MusicPlaybackState musicPlaybackState(int remoteControlClientPlaybackState) {
        switch (remoteControlClientPlaybackState) {
            case 1:
                return MusicPlaybackState.PLAYBACK_STOPPED;
            case 2:
                return MusicPlaybackState.PLAYBACK_PAUSED;
            case 3:
                return MusicPlaybackState.PLAYBACK_PLAYING;
            case 4:
                return MusicPlaybackState.PLAYBACK_FAST_FORWARDING;
            case 5:
                return MusicPlaybackState.PLAYBACK_REWINDING;
            case 6:
                return MusicPlaybackState.PLAYBACK_SKIPPING_TO_NEXT;
            case 7:
                return MusicPlaybackState.PLAYBACK_SKIPPING_TO_PREVIOUS;
            case 8:
                return MusicPlaybackState.PLAYBACK_BUFFERING;
            case 9:
                return MusicPlaybackState.PLAYBACK_ERROR;
            default:
                return MusicPlaybackState.PLAYBACK_NONE;
        }
    }

    private void logMetadata(MetadataEditor metadataEditor) {
        logger.v("- METADATA_KEY_ALBUM: " + metadataEditor.getString(1, ""));
        logger.v("- METADATA_KEY_ARTIST: " + metadataEditor.getString(2, ""));
        logger.v("- METADATA_KEY_TITLE: " + metadataEditor.getString(7, ""));
        logger.v("- METADATA_KEY_DURATION: " + metadataEditor.getLong(9, 0));
        logger.v("- BITMAP_KEY_ARTWORK: " + metadataEditor.getBitmap(100, null));
    }
}
