package com.navdy.client.app.framework.servicehandler;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Looper;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.CrashlyticsAppender;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.device.RemoteDevice.Listener;
import com.navdy.service.library.device.connection.Connection.ConnectionFailureCause;
import com.navdy.service.library.device.connection.Connection.DisconnectCause;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Message;
import java.util.List;

public class LocationTransmitter {
    public static final Logger sLogger = new Logger(LocationTransmitter.class);
    protected boolean isRunning;
    protected boolean mConnected;
    protected final Context mContext;
    private LocationListener mLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            LocationTransmitter.this.transmitLocation(location);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            LocationTransmitter.sLogger.v("status: " + provider + CrashlyticsAppender.SEPARATOR + status);
        }

        public void onProviderEnabled(String provider) {
            LocationTransmitter.sLogger.e("enabled: " + provider);
        }

        public void onProviderDisabled(String provider) {
            LocationTransmitter.sLogger.e("disabled: " + provider);
        }
    };
    protected LocationManager mLocationManager;
    protected final Looper mLocationReceiverLooper;
    protected final RemoteDevice mRemoteDevice;

    public LocationTransmitter(Context context, RemoteDevice remoteDevice) {
        HandlerThread handlerThread = new HandlerThread("location-transmitter");
        handlerThread.start();
        this.mLocationReceiverLooper = handlerThread.getLooper();
        this.mContext = context;
        this.mRemoteDevice = remoteDevice;
        remoteDevice.addListener(new Listener() {
            public void onDeviceConnecting(RemoteDevice device) {
            }

            public void onDeviceConnected(RemoteDevice device) {
                LocationTransmitter.this.mConnected = true;
            }

            public void onDeviceDisconnected(RemoteDevice device, DisconnectCause cause) {
                LocationTransmitter.this.mConnected = false;
            }

            public void onDeviceConnectFailure(RemoteDevice device, ConnectionFailureCause cause) {
            }

            public void onNavdyEventReceived(RemoteDevice device, NavdyEvent event) {
            }

            public void onNavdyEventReceived(RemoteDevice device, byte[] event) {
            }
        });
        this.mConnected = this.mRemoteDevice.isConnected();
        this.mLocationManager = (LocationManager) this.mContext.getSystemService("location");
    }

    protected boolean transmitLocation(Location androidLocation) {
        if (!this.mConnected) {
            sLogger.e("Can't transmit: not connected");
            return false;
        } else if (androidLocation == null) {
            sLogger.e("Invalid location - null");
            return false;
        } else {
            Message coordinate = new Coordinate(Double.valueOf(androidLocation.getLatitude()), Double.valueOf(androidLocation.getLongitude()), Float.valueOf(androidLocation.getAccuracy()), Double.valueOf(androidLocation.getAltitude()), Float.valueOf(androidLocation.getBearing()), Float.valueOf(androidLocation.getSpeed()), Long.valueOf(System.currentTimeMillis()), androidLocation.getProvider());
            sLogger.v("sending coordinate: lat=" + coordinate.latitude + ", lng=" + coordinate.longitude);
            return this.mRemoteDevice.postEvent(coordinate);
        }
    }

    public boolean start() {
        try {
            if (this.isRunning) {
                sLogger.i("location transmitter already started.");
                return false;
            } else if (NavdyApplication.getAppContext().checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                this.mLocationManager.requestLocationUpdates("gps", 0, 0.0f, this.mLocationListener, this.mLocationReceiverLooper);
                this.isRunning = true;
                sLogger.v("starting location updates");
                sendLastLocation();
                return true;
            } else {
                sLogger.v("permission for ACCESS_FINE_LOCATION failed");
                return false;
            }
        } catch (Throwable t) {
            sLogger.e(t);
            return false;
        }
    }

    public boolean stop() {
        if (this.isRunning) {
            try {
                if (NavdyApplication.getAppContext().checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                    this.mLocationManager.removeUpdates(this.mLocationListener);
                } else {
                    sLogger.v("mLocationManager.removeUpdates failed. Permission for ACCESS_FINE_LOCATION was not found.");
                }
            } catch (Throwable t) {
                sLogger.e(t);
            }
            sLogger.v("stopping location updates");
            this.isRunning = false;
            return true;
        }
        sLogger.e("location transmitter not running");
        return false;
    }

    private void sendLastLocation() {
        try {
            int permission = NavdyApplication.getAppContext().checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION");
            Location lastLocation = null;
            List<String> providers = this.mLocationManager.getAllProviders();
            if (permission == 0) {
                for (String str : providers) {
                    Location l = this.mLocationManager.getLastKnownLocation(str);
                    if (l != null) {
                        if (lastLocation == null) {
                            lastLocation = l;
                        } else if (l.getTime() > lastLocation.getTime()) {
                            lastLocation = l;
                        }
                    }
                }
                if (lastLocation == null) {
                    sLogger.v("no last location to transmit");
                    return;
                }
                sLogger.v("transmit last location:" + lastLocation);
                transmitLocation(lastLocation);
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
