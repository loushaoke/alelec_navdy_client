package com.navdy.client.app.framework.servicehandler;

import com.navdy.service.library.network.http.IHttpManager;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class NetworkStatusManager$$InjectAdapter extends Binding<NetworkStatusManager> implements Provider<NetworkStatusManager>, MembersInjector<NetworkStatusManager> {
    private Binding<IHttpManager> mHttpManager;

    public NetworkStatusManager$$InjectAdapter() {
        super("com.navdy.client.app.framework.servicehandler.NetworkStatusManager", "members/com.navdy.client.app.framework.servicehandler.NetworkStatusManager", false, NetworkStatusManager.class);
    }

    public void attach(Linker linker) {
        this.mHttpManager = linker.requestBinding("com.navdy.service.library.network.http.IHttpManager", NetworkStatusManager.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mHttpManager);
    }

    public NetworkStatusManager get() {
        NetworkStatusManager result = new NetworkStatusManager();
        injectMembers(result);
        return result;
    }

    public void injectMembers(NetworkStatusManager object) {
        object.mHttpManager = (IHttpManager) this.mHttpManager.get();
    }
}
