package com.navdy.client.app.ui.base;

import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public abstract class BaseEditActivity extends BaseToolbarActivity {
    OnCheckedChangeListener checkChangedListener = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            BaseEditActivity.this.somethingChanged = true;
        }
    };
    protected boolean saveOnExit = true;
    protected boolean somethingChanged = false;

    protected abstract void saveChanges();

    protected void discardChanges() {
    }

    protected void onPause() {
        if (this.saveOnExit) {
            if (this.somethingChanged) {
                saveChanges();
            }
            this.somethingChanged = false;
        }
        super.onPause();
    }

    protected void onResume() {
        super.onResume();
        this.saveOnExit = true;
        this.somethingChanged = false;
    }

    protected void initCompoundButton(CompoundButton button, boolean isChecked) {
        initCompoundButton(button, isChecked, this.checkChangedListener);
    }

    protected void initCompoundButton(CompoundButton button, boolean isChecked, OnCheckedChangeListener listener) {
        if (button != null) {
            button.setChecked(isChecked);
            button.setOnCheckedChangeListener(listener);
        }
    }
}
