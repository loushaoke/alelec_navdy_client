package com.navdy.client.app.ui.customviews;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;
import com.alelec.navdyclient.R;

public class FluctuatorAnimatorView extends View {
    private ValueAnimator alphaAnimator;
    private int animationDelay;
    private int animationDuration;
    private DefaultAnimationListener animationListener;
    private AnimatorSet animatorSet;
    float currentCircle;
    private float endRadius;
    private Handler handler;
    private LinearInterpolator interpolator;
    private Paint paint;
    private int paintColor;
    private ValueAnimator radiusAnimator;
    private float startRadius;
    public Runnable startRunnable;
    private float strokeWidth;

    public FluctuatorAnimatorView(Context context) {
        this(context, null);
    }

    public FluctuatorAnimatorView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FluctuatorAnimatorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.interpolator = new LinearInterpolator();
        this.handler = new Handler(Looper.getMainLooper());
        this.animationListener = new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                FluctuatorAnimatorView.this.handler.postDelayed(FluctuatorAnimatorView.this.startRunnable, (long) FluctuatorAnimatorView.this.animationDelay);
            }
        };
        this.startRunnable = new Runnable() {
            public void run() {
                FluctuatorAnimatorView.this.animatorSet.start();
            }
        };
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FluctuatorAnimatorView, defStyleAttr, 0);
        if (a != null) {
            this.paintColor = a.getColor(0, 0);
            this.startRadius = a.getDimension(1, 0.0f);
            this.endRadius = a.getDimension(2, 0.0f);
            this.strokeWidth = a.getDimension(3, 0.0f);
            this.animationDuration = a.getInteger(4, 0);
            this.animationDelay = a.getInteger(5, 0);
            a.recycle();
        }
        this.paint = new Paint();
        this.paint.setStrokeWidth(this.strokeWidth);
        this.paint.setStyle(Style.STROKE);
        this.paint.setAntiAlias(true);
        this.paint.setColor(this.paintColor);
        this.animatorSet = new AnimatorSet();
        this.animatorSet.setDuration((long) this.animationDuration);
        this.radiusAnimator = ValueAnimator.ofFloat(new float[]{this.startRadius, this.endRadius});
        this.alphaAnimator = ValueAnimator.ofFloat(new float[]{1.0f, 0.0f});
        this.animatorSet.setInterpolator(this.interpolator);
        this.radiusAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                FluctuatorAnimatorView.this.currentCircle = ((Float) animation.getAnimatedValue()).floatValue();
                FluctuatorAnimatorView.this.invalidate();
            }
        });
        this.alphaAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                FluctuatorAnimatorView.this.setAlpha(((Float) animation.getAnimatedValue()).floatValue());
            }
        });
        this.animatorSet.playTogether(new Animator[]{this.radiusAnimator, this.alphaAnimator});
    }

    public void setColor(int paintColor) {
        this.paint.setColor(paintColor);
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, this.currentCircle, this.paint);
    }

    public void start() {
        stop();
        this.animatorSet.addListener(this.animationListener);
        this.animatorSet.start();
    }

    public void stop() {
        this.handler.removeCallbacks(this.startRunnable);
        if (this.animatorSet.isRunning()) {
            this.animatorSet.removeAllListeners();
            this.animatorSet.cancel();
        }
    }
}
