package com.navdy.client.app.ui.settings;

import android.widget.Button;
import butterknife.ButterKnife.Finder;
import com.alelec.navdyclient.R;
import com.navdy.client.app.ui.customviews.MultipleChoiceLayout;

public class HFPAudioDelaySettingsActivity$$ViewInjector {
    public static void inject(Finder finder, HFPAudioDelaySettingsActivity target, Object source) {
        target.btnPlaySequence = (Button) finder.findRequiredView(source, R.id.play_sequence, "field 'btnPlaySequence'");
        target.multipleChoiceLayout = (MultipleChoiceLayout) finder.findRequiredView(source, R.id.sequence_delay_choice_layout, "field 'multipleChoiceLayout'");
    }

    public static void reset(HFPAudioDelaySettingsActivity target) {
        target.btnPlaySequence = null;
        target.multipleChoiceLayout = null;
    }
}
