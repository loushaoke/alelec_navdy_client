package com.navdy.client.app.ui.settings;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.MainThread;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.here.odnp.config.OdnpConfigStatic;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.PathManager;
import com.navdy.client.app.framework.util.CarMdClient;
import com.navdy.client.app.framework.util.ImageUtils;
import com.navdy.client.app.framework.util.ImageUtils.StreamFactory;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SupportTicketService;
import com.navdy.client.app.framework.util.SupportTicketService.OnLogCollectedInterface;
import com.navdy.client.app.framework.util.SupportTicketService.SubmitTicketFinishedEvent;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen.Settings;
import com.navdy.client.app.ui.PhotoUtils;
import com.navdy.client.app.ui.WebViewActivity;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.customviews.CustomSpinner;
import com.navdy.client.app.ui.customviews.CustomSpinner.OnSpinnerStateChangedListener;
import com.navdy.service.library.events.obd.ObdStatusResponse;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Subscribe;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.UUID;
import org.json.JSONObject;

public class ContactUsActivity extends BaseToolbarActivity implements OnLogCollectedInterface {
    private static final int DELAY_BEFORE_FINISHING_ACTIVITY = 3000;
    private static final int DIALOG_PHOTO_LIMIT_EXCEEDED_ID = 1;
    private static final int DIALOG_PICK_FROM_GALLERY_OPTION = 0;
    private static final int DIALOG_TAKE_PHOTO_OPTION = 1;
    public static final String DISPLAY_LOG_CONTAIN_FILTER = "display_log";
    public static final String EXTRA_DEFAULT_PROBLEM_TYPE = "default_problem_type";
    private static final int FILE_SIZE_LIMIT = 2097152;
    private static final int GALLERY_IMAGE_SIZE = 32;
    private static final int MAX_PIXEL_DIMENSION = 1280;
    private static final int PHOTO_ATTACHMENT_LIMIT = 5;
    private static final int STORAGE_PERMISSIONS_REQUEST_CODE = 3;
    public static final String TEMP_PHOTO_FILENAME = "temp_photo_filename";
    private TextView attachPhotoTextView;
    private ArrayList<File> attachments = new ArrayList();
    private String currentTicketId = "";
    private ArrayList<WeakReference<Dialog>> dialogs = new ArrayList();
    private Runnable dismissDialogsAndFinishRunnable = new Runnable() {
        public void run() {
            ContactUsActivity.this.dismissAllDialogs();
            ContactUsActivity.this.finish();
        }
    };
    private boolean displayLogAttached;
    private String email;
    private EditText emailAddress;
    private Runnable failedToGetStorageRunnable = new Runnable() {
        public void run() {
            ContactUsActivity.this.logger.v("failed to get storage permission for logs");
            ContactUsActivity.this.showGetStoragePermissionDialog();
        }
    };
    private boolean folderHasBeenCreatedForThisTicket;
    private Iterator<ImageView> imageViewIterator;
    private Toolbar myToolbar;
    private int photoAttachmentCount = 0;
    private EditText problemInput;
    private CustomSpinner problemTypes;
    private String tempPhotoFilename;
    private boolean userWantsLogs;
    private String vin;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.settings_contact_us);
        this.myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        new ToolbarBuilder().title((int) R.string.contact_us).build();
        this.emailAddress = (EditText) findViewById(R.id.enter_email_address);
        this.problemTypes = (CustomSpinner) findViewById(R.id.problem_types);
        this.problemInput = (EditText) findViewById(R.id.problem_input);
        this.attachPhotoTextView = (TextView) findViewById(R.id.attach_image);
        setupGallery();
        if (this.problemTypes != null) {
            final ArrayAdapter<CharSequence> introAdapter = ArrayAdapter.createFromResource(this, R.array.settings_report_a_problem_select_a_topic, 17367048);
            introAdapter.setDropDownViewResource(17367049);
            final ArrayAdapter<CharSequence> problemAdapter = ArrayAdapter.createFromResource(this, R.array.settings_report_a_problem_problem_types, 17367048);
            problemAdapter.setDropDownViewResource(17367049);
            this.problemTypes.setAdapter(introAdapter);
            this.problemTypes.setListener(new OnSpinnerStateChangedListener() {
                public void spinnerOpened() {
                    ContactUsActivity.this.problemTypes.setAdapter(problemAdapter);
                }

                public void spinnerClosed() {
                    ContactUsActivity.this.logger.v("getSelectedItemPosition: " + ContactUsActivity.this.problemTypes.getSelectedItemPosition());
                    ContactUsActivity.this.updateSubmitButton();
                    if (ContactUsActivity.this.problemTypes.getSelectedItemPosition() == 0) {
                        ContactUsActivity.this.problemTypes.setAdapter(introAdapter);
                    }
                    if (ContactUsActivity.this.problemTypes.getSelectedItemPosition() == 9) {
                        ContactUsActivity.this.problemInput.setHint(R.string.settings_report_a_problem_log_attachment_hint);
                    } else {
                        ContactUsActivity.this.problemInput.setHint(R.string.settings_report_a_problem_hint);
                    }
                }
            });
            Intent intent = getIntent();
            if (intent != null) {
                int defaultProblemType = intent.getIntExtra(EXTRA_DEFAULT_PROBLEM_TYPE, -1);
                if (defaultProblemType > 0) {
                    this.problemTypes.setAdapter(problemAdapter);
                    this.problemTypes.setSelection(defaultProblemType);
                }
            }
        }
        SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
        this.userWantsLogs = SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.AUTO_ATTACH_LOGS_ENABLED, false);
        if (this.emailAddress != null) {
            this.email = SupportTicketService.getEmail();
            if (StringUtils.isEmptyAfterTrim(this.email)) {
                this.email = customerPrefs.getString("email", "");
            }
            this.emailAddress.setText(this.email);
            this.emailAddress.setSelection(this.email.length());
            if (ZendeskConfig.INSTANCE.isInitialized()) {
                this.emailAddress.setEnabled(false);
            } else {
                this.emailAddress.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    public void afterTextChanged(Editable s) {
                        ContactUsActivity.this.email = ContactUsActivity.this.emailAddress.getText().toString();
                        ContactUsActivity.this.updateSubmitButton();
                    }
                });
            }
        }
        this.problemInput.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                ContactUsActivity.this.updateSubmitButton();
            }
        });
        new SettingsLogger().logConfiguration();
    }

    private void updateSubmitButton() {
        if (emailIsValid() && messageIsValid() && problemTypeIsValid()) {
            showSubmitButton();
        } else {
            disableSubmitButton();
        }
    }

    private boolean emailIsValid() {
        return this.emailAddress != null && StringUtils.isValidEmail(this.emailAddress.getText());
    }

    private boolean messageIsValid() {
        return this.problemInput != null && this.problemInput.getText().length() > 0;
    }

    private boolean problemTypeIsValid() {
        return this.problemTypes != null && this.problemTypes.getSelectedItemPosition() > 0;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        addSubmitButtonToMenu(menu);
        disableSubmitButton();
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.btn_submit) {
            createTicket();
        }
        return super.onOptionsItemSelected(item);
    }

    private void addSubmitButtonToMenu(Menu menu) {
        menu.add(0, R.id.btn_submit, 0, R.string.settings_report_a_problem_submit).setShowAsAction(2);
    }

    private void hideSubmitButton() {
        if (this.myToolbar != null) {
            Menu menu = this.myToolbar.getMenu();
            if (menu != null) {
                menu.removeItem(R.id.btn_submit);
            }
        }
    }

    private void showSubmitButton() {
        if (this.myToolbar != null) {
            Menu menu = this.myToolbar.getMenu();
            if (menu != null) {
                menu.removeItem(R.id.btn_submit);
                addSubmitButtonToMenu(menu);
            }
        }
    }

    private void disableSubmitButton() {
        if (this.myToolbar != null) {
            Menu menu = this.myToolbar.getMenu();
            if (menu != null) {
                MenuItem sendButton = menu.findItem(R.id.btn_submit);
                if (sendButton != null) {
                    sendButton.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            ContactUsActivity.this.showMoreDetailsRequiredDialog();
                            return true;
                        }
                    });
                }
            }
        }
    }

    protected void onResume() {
        super.onResume();
        Tracker.tagScreen(Settings.CONTACT);
    }

    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if (!StringUtils.isEmptyAfterTrim(this.tempPhotoFilename)) {
            savedInstanceState.putString(TEMP_PHOTO_FILENAME, this.tempPhotoFilename);
        }
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (StringUtils.isEmptyAfterTrim(this.tempPhotoFilename)) {
            this.tempPhotoFilename = savedInstanceState.getString(TEMP_PHOTO_FILENAME, this.tempPhotoFilename);
        }
    }

    @Subscribe
    public void onObdStatusResponse(ObdStatusResponse obdStatusResponse) {
        this.vin = obdStatusResponse.vin != null ? obdStatusResponse.vin : getResources().getString(R.string.unknown);
    }

    private void createTicket() {
        this.logger.v("create a Request Ticket");
        if (findViewById(R.id.master_layout) == null || (emailIsValid() && problemTypeIsValid() && messageIsValid())) {
            SystemUtils.dismissKeyboard(this);
            if (this.userWantsLogs) {
                requestStoragePermission(new Runnable() {
                    public void run() {
                        ContactUsActivity.this.showProgressDialog();
                        SupportTicketService.collectLogs(ContactUsActivity.this);
                    }
                }, this.failedToGetStorageRunnable);
            } else {
                requestStoragePermission(new Runnable() {
                    public void run() {
                        ContactUsActivity.this.showSingleChoiceDialogWithMessage();
                    }
                }, this.failedToGetStorageRunnable);
            }
        } else if (!isFinishing() && !isDestroyed()) {
            showMoreDetailsRequiredDialog();
        }
    }

    @MainThread
    private void showSuccessfulTicketDialog() {
        SystemUtils.ensureOnMainThread();
        if (!BaseActivity.isEnding(this)) {
            Builder builder = new Builder(this);
            builder.setTitle(R.string.request_sent);
            builder.setMessage(R.string.request_sent_to_navdy);
            builder.setPositiveButton(R.string.got_it, null);
            builder.setOnDismissListener(new OnDismissListener() {
                public void onDismiss(DialogInterface dialogInterface) {
                    ContactUsActivity.this.finish();
                }
            });
            AlertDialog dialog = builder.create();
            this.dialogs.add(new WeakReference(dialog));
            dialog.show();
        }
    }

    public void showSingleChoiceDialogWithMessage() {
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_for_attaching_logs, null);
        AlertDialog dialog = new Builder(this).setView(dialogView).create();
        ((TextView) dialogView.findViewById(R.id.dialog_detail)).setText(StringUtils.fromHtml((int) R.string.attach_logs_desc));
        this.dialogs.add(new WeakReference(dialog));
        dialog.show();
    }

    public void onPrivacyPolicyClick(View view) {
        Intent browserIntent = new Intent(getApplicationContext(), WebViewActivity.class);
        browserIntent.putExtra("type", WebViewActivity.PRIVACY);
        startActivity(browserIntent);
    }

    public void onOnceClick(View view) {
        dismissAllDialogs();
        this.userWantsLogs = true;
        showProgressDialog();
        SupportTicketService.collectLogs(this);
    }

    public void onNoLogsClick(View view) {
        dismissAllDialogs();
        startSubmitTicketService();
    }

    public void onAlwaysClick(View view) {
        SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.AUTO_ATTACH_LOGS_ENABLED, true).apply();
        onOnceClick(view);
    }

    @MainThread
    private void showGetStoragePermissionDialog() {
        SystemUtils.ensureOnMainThread();
        if (!BaseActivity.isEnding(this)) {
            Builder builder = new Builder(this);
            builder.setCancelable(true);
            builder.setTitle(R.string.we_need_storage_permission_for_tickets);
            builder.setMessage(R.string.how_to_enable_storage);
            builder.setPositiveButton(R.string.go_to_permission_settings, new OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent();
                    intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
                    intent.setData(Uri.parse("package:" + ContactUsActivity.this.getPackageName()));
                    ContactUsActivity.this.startActivityForResult(intent, 3);
                }
            });
            builder.setNegativeButton(R.string.cancel_request, new OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            this.dialogs.add(new WeakReference(dialog));
            dialog.show();
        }
    }

    @MainThread
    private void showAttachPhotoDialog() {
        SystemUtils.ensureOnMainThread();
        if (!BaseActivity.isEnding(this)) {
            if (this.photoAttachmentCount >= 5) {
                showSimpleDialog(1, getString(R.string.max_photos_exceeded), getString(R.string.max_photos_exceeded_explanation));
                return;
            }
            CharSequence[] options = getResources().getStringArray(R.array.settings_contact_us_photo_options);
            Builder builder = new Builder(this);
            builder.setCancelable(true);
            builder.setTitle(R.string.attach_image_title);
            builder.setItems(options, new OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    switch (i) {
                        case 0:
                            PhotoUtils.getPhotoFromGallery(ContactUsActivity.this);
                            return;
                        case 1:
                            ContactUsActivity.this.tempPhotoFilename = IOUtils.getTempFilename();
                            PhotoUtils.takePhoto(ContactUsActivity.this, ContactUsActivity.this.logger, ContactUsActivity.this.tempPhotoFilename);
                            return;
                        default:
                            return;
                    }
                }
            });
            AlertDialog dialog = builder.create();
            this.dialogs.add(new WeakReference(dialog));
            dialog.show();
        }
    }

    @MainThread
    private void showMoreDetailsRequiredDialog() {
        SystemUtils.ensureOnMainThread();
        if (!BaseActivity.isEnding(this)) {
            Builder builder = new Builder(this);
            builder.setTitle(R.string.more_details_required);
            StringBuilder problemsString = new StringBuilder(getString(R.string.we_need_a_bit_more));
            if (!emailIsValid()) {
                problemsString.append(getString(R.string.enter_an_accurate_email_bulletpoint));
            }
            if (!problemTypeIsValid()) {
                problemsString.append(getString(R.string.select_a_topic_bulletpoint));
            }
            if (!messageIsValid()) {
                problemsString.append(getString(R.string.describe_the_problem_bulletpoint));
            }
            builder.setMessage(problemsString);
            builder.setPositiveButton(R.string.ok, null);
            AlertDialog dialog = builder.create();
            this.dialogs.add(new WeakReference(dialog));
            dialog.show();
        }
    }

    public void attachPhotoToZendesk(View view) {
        showAttachPhotoDialog();
    }

    public void onActivityResult(int requestCode, int resultCode, final Intent responseIntent) {
        if (resultCode == -1) {
            if (requestCode == 1) {
                requestStoragePermission(new Runnable() {
                    public void run() {
                        File tempFile = null;
                        if (responseIntent != null) {
                            Bundle bundle = responseIntent.getExtras();
                            if (bundle != null) {
                                Bitmap image = (Bitmap) bundle.get(CarMdClient.DATA);
                                ContactUsActivity.this.logger.d("image status: " + image);
                                if (image != null) {
                                    tempFile = new File(Environment.getExternalStorageDirectory(), ContactUsActivity.this.tempPhotoFilename);
                                    ContactUsActivity.this.compressBitmapToFile(image, tempFile, CompressFormat.JPEG, 85);
                                }
                            }
                        }
                        if (tempFile == null && !StringUtils.isEmptyAfterTrim(ContactUsActivity.this.tempPhotoFilename)) {
                            ContactUsActivity.this.logger.d("retrieving photo from storage");
                            tempFile = IOUtils.getTempFile(ContactUsActivity.this.logger, ContactUsActivity.this.tempPhotoFilename);
                        }
                        if (tempFile != null && tempFile.exists()) {
                            Bitmap bitmap = ImageUtils.getScaledBitmap(tempFile, (int) ContactUsActivity.MAX_PIXEL_DIMENSION, (int) ContactUsActivity.MAX_PIXEL_DIMENSION);
                            if (bitmap == null) {
                                ContactUsActivity.this.logger.e("bitmap was null");
                                ContactUsActivity.this.showFailureSnackBar();
                                return;
                            }
                            if (tempFile.exists() && tempFile.length() > 2097152) {
                                ContactUsActivity.this.compressBitmapToFile(bitmap, tempFile, CompressFormat.JPEG, 85);
                            }
                            File photoFile = IOUtils.getTempFile(ContactUsActivity.this.logger, ContactUsActivity.this.tempPhotoFilename);
                            if (photoFile != null) {
                                ContactUsActivity.this.attachments.add(photoFile);
                                ContactUsActivity.this.showSuccessSnackBar();
                                ContactUsActivity.this.incrementPhotoAttachments();
                                ContactUsActivity.this.attachPhotoTextView.setText(R.string.attach_another_photo);
                                if (ContactUsActivity.this.imageViewIterator != null && ContactUsActivity.this.imageViewIterator.hasNext()) {
                                    ContactUsActivity.this.addPhotoToGallery(bitmap);
                                }
                            } else {
                                ContactUsActivity.this.showFailureSnackBar();
                            }
                            bitmap.recycle();
                        }
                    }
                }, new Runnable() {
                    public void run() {
                        BaseActivity.showLongToast(R.string.storage_permission_not_granted, new Object[0]);
                    }
                });
            } else if (requestCode == 2 && responseIntent != null && responseIntent.getData() != null) {
                try {
                    final Uri uri = responseIntent.getData();
                    if (uri != null) {
                        Bitmap bitmap = ImageUtils.getScaledBitmap(new StreamFactory() {
                            public InputStream getInputStream() throws FileNotFoundException {
                                return ContactUsActivity.this.getContentResolver().openInputStream(uri);
                            }
                        }, (int) MAX_PIXEL_DIMENSION, (int) MAX_PIXEL_DIMENSION);
                        if (bitmap != null) {
                            String fileName = IOUtils.getTempFilename();
                            Tracker.saveBitmapToInternalStorage(bitmap, fileName);
                            File galleryPhoto = new File(NavdyApplication.getAppContext().getFilesDir() + File.separator, fileName);
                            if (galleryPhoto.exists()) {
                                this.attachments.add(galleryPhoto);
                                showSuccessSnackBar();
                                incrementPhotoAttachments();
                                this.attachPhotoTextView.setText(R.string.attach_another_photo);
                                if (this.imageViewIterator != null && this.imageViewIterator.hasNext()) {
                                    addPhotoToGallery(bitmap);
                                }
                            } else {
                                this.logger.v("photo file is null");
                                showFailureSnackBar();
                            }
                            bitmap.recycle();
                        }
                    }
                } catch (Exception e) {
                    this.logger.e("failed to get photo gallery image: " + e);
                    showFailureSnackBar();
                }
            } else if (requestCode == 3) {
                createTicket();
            }
        }
        super.onActivityResult(requestCode, resultCode, responseIntent);
    }

    private void compressBitmapToFile(Bitmap bitmap, File outputFile, CompressFormat format, int quality) {
        Exception e;
        Throwable th;
        BufferedOutputStream outputStream = null;
        try {
            BufferedOutputStream outputStream2 = new BufferedOutputStream(new FileOutputStream(outputFile));
            try {
                bitmap.compress(format, quality, outputStream2);
                IOUtils.closeStream(outputStream2);
                outputStream = outputStream2;
            } catch (Exception e2) {
                e = e2;
                outputStream = outputStream2;
                try {
                    this.logger.e("Exception compressing bitmap: " + e);
                    IOUtils.closeStream(outputStream);
                } catch (Throwable th2) {
                    th = th2;
                    IOUtils.closeStream(outputStream);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                outputStream = outputStream2;
                IOUtils.closeStream(outputStream);
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            this.logger.e("Exception compressing bitmap: " + e);
            IOUtils.closeStream(outputStream);
        }
    }

    private void showFailureSnackBar() {
        if (!isFinishing()) {
            hideProgressDialog();
            View view = findViewById(R.id.master_layout);
            if (view != null) {
                Snackbar.make(view, (int) R.string.photo_upload_failed, 0).setAction((int) R.string.retry_button, new View.OnClickListener() {
                    public void onClick(View view) {
                        ContactUsActivity.this.showAttachPhotoDialog();
                    }
                }).show();
            }
        }
    }

    private void showSuccessSnackBar() {
        if (!isFinishing()) {
            hideProgressDialog();
            View view = findViewById(R.id.master_layout);
            if (view != null) {
                Snackbar.make(view, (int) R.string.photo_upload_succeeded, 0).show();
            }
        }
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (this.problemTypes != null && this.problemTypes.hasBeenOpened() && hasFocus) {
            this.problemTypes.spinnerClosed();
        }
    }

    private JSONObject createJSONFromTicketContents(String ticketDescription, String vin) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ZendeskConstants.TICKET_TYPE, getTagFromProblemType());
            jsonObject.put(ZendeskConstants.TICKET_DESCRIPTION, ticketDescription);
            jsonObject.put(ZendeskConstants.TICKET_VIN, vin);
            if (!StringUtils.isEmptyAfterTrim(this.emailAddress.getText())) {
                jsonObject.put(ZendeskConstants.TICKET_EMAIL, this.emailAddress.getText().toString());
            }
            jsonObject.put(ZendeskConstants.TICKET_HAS_DISPLAY_LOG, this.displayLogAttached);
            if (!this.userWantsLogs || this.displayLogAttached) {
                jsonObject.put(ZendeskConstants.TICKET_ACTION, ZendeskConstants.ACTION_UPLOAD_DELETE);
                return jsonObject;
            }
            jsonObject.put(ZendeskConstants.TICKET_ACTION, ZendeskConstants.ACTION_UPLOAD_AWAIT_HUD_LOG);
            return jsonObject;
        } catch (Throwable e) {
            this.logger.e("Failed to create JSON from Ticket Contents: " + e);
            return null;
        }
    }

    private String getTagFromProblemType() {
        return getResources().getStringArray(R.array.settings_report_a_problem_problem_types_tags)[this.problemTypes.getSelectedItemPosition()];
    }

    private void createReadMeIfDoesNotExist() {
        File readMe = new File(PathManager.getInstance().getTicketFolderPath() + File.separator + SupportTicketService.TICKET_FOLDER_README_FILENAME);
        if (!readMe.exists()) {
            try {
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(readMe, true));
                bufferedWriter.append(getString(R.string.zendesk_ticket_readme_message));
                bufferedWriter.newLine();
                bufferedWriter.close();
            } catch (Exception e) {
                this.logger.e("There was an exception creating the ReadMe file: " + e);
            }
        }
    }

    private void setupGallery() {
        ArrayList<ImageView> imageViewArrayList = new ArrayList();
        imageViewArrayList.add((ImageView) findViewById(R.id.attach_photo_icon));
        imageViewArrayList.add((ImageView) findViewById(R.id.attach_photo_icon_2));
        imageViewArrayList.add((ImageView) findViewById(R.id.attach_photo_icon_3));
        imageViewArrayList.add((ImageView) findViewById(R.id.attach_photo_icon_4));
        imageViewArrayList.add((ImageView) findViewById(R.id.attach_photo_icon_5));
        this.imageViewIterator = imageViewArrayList.iterator();
    }

    private void addPhotoToGallery(Bitmap attachmentPhoto) {
        Bitmap icon = Bitmap.createScaledBitmap(attachmentPhoto, 32, 32, true);
        if (icon != null) {
            ImageView imageView = (ImageView) this.imageViewIterator.next();
            imageView.setImageBitmap(icon);
            imageView.setVisibility(VISIBLE);
        }
    }

    @Subscribe
    public void onReceiveSubmitTicketFinishedEvent(SubmitTicketFinishedEvent submitTicketFinishedEvent) {
        if (!submitTicketFinishedEvent.getTicketId().equals(this.currentTicketId)) {
            return;
        }
        if (submitTicketFinishedEvent.getConditionsMet()) {
            this.dismissDialogsAndFinishRunnable.run();
        } else {
            this.handler.postDelayed(this.dismissDialogsAndFinishRunnable, OdnpConfigStatic.CELL_NO_CHANGE_LIMITER_TIME);
        }
    }

    protected void onPause() {
        dismissAllDialogs();
        super.onPause();
    }

    private void dismissAllDialogs() {
        ListIterator<WeakReference<Dialog>> dialogIterator = this.dialogs.listIterator();
        while (dialogIterator.hasNext()) {
            WeakReference<Dialog> weakReferenceToDialog = (WeakReference) dialogIterator.next();
            if (weakReferenceToDialog.get() != null) {
                Dialog dialog = (Dialog) weakReferenceToDialog.get();
                if (dialog != null) {
                    dialog.dismiss();
                }
            } else {
                dialogIterator.remove();
            }
        }
    }

    public void onLogCollectionSucceeded(ArrayList<File> logAttachments) {
        this.logger.i("onLogCollectionSucceeded: " + logAttachments.size());
        if (!(logAttachments.isEmpty() || this.attachments == null)) {
            this.attachments.addAll(logAttachments);
            Iterator it = this.attachments.iterator();
            while (it.hasNext()) {
                if (((File) it.next()).getName().contains("display_log")) {
                    this.displayLogAttached = true;
                    break;
                }
            }
            if (!this.displayLogAttached) {
                this.logger.d("display log was not found");
            }
        }
        runOnUiThread(new Runnable() {
            public void run() {
                ContactUsActivity.this.startSubmitTicketService();
            }
        });
    }

    public void onLogCollectionFailed() {
        this.logger.i("onLogCollectionFailed");
        runOnUiThread(new Runnable() {
            public void run() {
                ContactUsActivity.this.startSubmitTicketService();
            }
        });
    }

    private void startSubmitTicketService() {
        String ticketDesc = this.problemInput.getText().toString();
        hideProgressDialog();
        if (this.folderHasBeenCreatedForThisTicket) {
            this.logger.v("folder for this ticket already exists");
        } else {
            if (SupportTicketService.createFolderForTicket(this.attachments, createJSONFromTicketContents(ticketDesc, this.vin))) {
                this.folderHasBeenCreatedForThisTicket = true;
                createReadMeIfDoesNotExist();
            } else {
                this.logger.e("creating folder for ticket failed");
                return;
            }
        }
        Context context = NavdyApplication.getAppContext();
        Intent i = new Intent(context, SupportTicketService.class);
        i.putExtra(SupportTicketService.EXTRA_RESET_COUNTER, true);
        i.putExtra(SupportTicketService.EXTRA_EMAIL, this.email);
        this.currentTicketId = UUID.randomUUID().toString();
        i.putExtra(SupportTicketService.EXTRA_TICKET_RANDOM_ID, this.currentTicketId);
        context.startService(i);
        hideSubmitButton();
        showSuccessfulTicketDialog();
    }

    public void onSelectProblemInputClick(View view) {
        this.problemInput.requestFocus();
        ((InputMethodManager) getSystemService("input_method")).toggleSoftInput(2, 0);
    }

    public void incrementPhotoAttachments() {
        this.photoAttachmentCount++;
        if (this.photoAttachmentCount >= 5) {
            this.attachPhotoTextView.setTextColor(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.grey_hint));
        }
    }
}
