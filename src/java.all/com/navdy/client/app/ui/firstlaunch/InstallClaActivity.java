package com.navdy.client.app.ui.firstlaunch;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.alelec.navdyclient.R;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install;
import com.navdy.client.app.ui.base.BaseActivity;

public class InstallClaActivity extends BaseActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fle_install_cla);
        loadImage(R.id.illustration, R.drawable.image_12_v_fpo);
    }

    protected void onResume() {
        super.onResume();
        hideSystemUI();
        Tracker.tagScreen(Install.PLUG_CLA);
    }

    public void onNextClick(View view) {
        Intent intent = new Intent(getApplicationContext(), InstallActivity.class);
        intent.putExtra("extra_step", R.layout.fle_install_tidying_up);
        startActivity(intent);
    }

    public void onBackClick(View view) {
        finish();
    }
}
