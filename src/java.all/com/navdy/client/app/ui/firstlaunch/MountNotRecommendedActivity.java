package com.navdy.client.app.ui.firstlaunch;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.models.MountInfo.MountType;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;

public class MountNotRecommendedActivity extends BaseToolbarActivity {
    String box = SettingsUtils.getSharedPreferences().getString(SettingsConstants.BOX, "Old_Box");
    SharedPreferences customerPrefs;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fle_mount_not_recommended);
        this.customerPrefs = SettingsUtils.getCustomerPreferences();
        new ToolbarBuilder().title(SettingsUtils.getCarYearMakeModelString()).build();
    }

    protected void onResume() {
        super.onResume();
        hideSystemUI();
        TextView title = (TextView) findViewById(R.id.you_will_need_the_mount_kit_title);
        TextView desc = (TextView) findViewById(R.id.you_will_need_the_mount_kit_description);
        String modelString = this.customerPrefs.getString(ProfilePreferences.CAR_MODEL, "");
        String mount = getIntent().getStringExtra("extra_mount");
        if (StringUtils.isEmptyAfterTrim(mount) || StringUtils.equalsOrBothEmptyAfterTrim(mount, MountType.SHORT.getValue())) {
            Tracker.tagScreen(Install.SHORT_MOUNT_NOT_RECOMMENDED);
            if (StringUtils.equalsOrBothEmptyAfterTrim(this.box, "Old_Box")) {
                loadImage(R.id.illustration, R.drawable.img_installation_lens_2016_short_low);
            } else {
                loadImage(R.id.illustration, R.drawable.img_installation_lens_2017_short_low);
            }
            if (title != null) {
                title.setText(getString(R.string.the_short_mount_is_not_recommended, new Object[]{modelString}));
            }
            if (desc != null) {
                desc.setText(R.string.the_short_mount_is_not_recommended_description);
                return;
            }
            return;
        }
        Tracker.tagScreen(Install.MEDIUM_TALL_MOUNT_NOT_RECOMMENDED);
        loadImage(R.id.illustration, R.drawable.img_installation_lens_medium_tall_low);
        if (title != null) {
            title.setText(getString(R.string.the_medium_and_tall_mounts_are_not_recommended, new Object[]{modelString}));
        }
        if (desc != null) {
            desc.setText(R.string.the_medium_and_tall_mounts_are_not_recommended_description);
        }
    }

    public void onContinueWithShortClick(View view) {
        String mount = getIntent().getStringExtra("extra_mount");
        Intent intent = new Intent(getApplicationContext(), InstallActivity.class);
        intent.putExtra("extra_step", R.layout.fle_install_overview);
        intent.putExtra("extra_mount", mount);
        if (StringUtils.isEmptyAfterTrim(mount) || StringUtils.equalsOrBothEmptyAfterTrim(mount, MountType.SHORT.getValue())) {
            Tracker.tagEvent(Event.Install.SHORT_MOUNT_NOT_RECOMMENDED_CONTINUE);
        } else {
            Tracker.tagEvent(Event.Install.MEDIUM_TALL_MOUNT_NOT_RECOMMENDED_CONTINUE);
            if (StringUtils.equalsOrBothEmptyAfterTrim(this.box, "New_Box")) {
                intent = new Intent(getApplicationContext(), YouWillNeedTheMountKitActivity.class);
            }
        }
        startActivity(intent);
        finish();
    }

    public void onBackClick(View view) {
        onBackPressed();
    }
}
