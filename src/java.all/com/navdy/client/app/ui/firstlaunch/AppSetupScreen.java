package com.navdy.client.app.ui.firstlaunch;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class AppSetupScreen implements Parcelable {
    public static final Creator<AppSetupScreen> CREATOR = new Creator<AppSetupScreen>() {
        public AppSetupScreen createFromParcel(Parcel in) {
            return new AppSetupScreen(in);
        }

        public AppSetupScreen[] newArray(int size) {
            return new AppSetupScreen[size];
        }
    };
    public int blueTextRes;
    public int buttonFailRes;
    public int buttonRes;
    public int descriptionFailRes;
    public int descriptionRes;
    public int hudRes;
    public int hudResFail;
    public boolean isMandatory;
    public ScreenType screenType = ScreenType.UNKNOWN;
    public int titleFailRes;
    public int titleRes;

    public enum ScreenType {
        UNKNOWN(-1),
        PROFILE(0),
        BLUETOOTH(1),
        BLUETOOTH_SUCCESS(2),
        NOTIFICATIONS(3),
        ACCESS_FINE_LOCATION(4),
        USE_MICROPHONE(5),
        READ_CONTACTS(6),
        RECEIVE_SMS(7),
        CALL_PHONE(8),
        READ_CALENDAR(9),
        WRITE_EXTERNAL_STORAGE(10),
        END(11);
        
        int type;

        private ScreenType(int type) {
            this.type = type;
        }

        public static ScreenType fromValue(int type) {
            switch (type) {
                case 0:
                    return PROFILE;
                case 1:
                    return BLUETOOTH;
                case 2:
                    return BLUETOOTH_SUCCESS;
                case 3:
                    return NOTIFICATIONS;
                case 4:
                    return ACCESS_FINE_LOCATION;
                case 5:
                    return READ_CONTACTS;
                case 6:
                    return RECEIVE_SMS;
                case 7:
                    return CALL_PHONE;
                case 8:
                    return READ_CALENDAR;
                case 9:
                    return WRITE_EXTERNAL_STORAGE;
                case 10:
                    return END;
                default:
                    return UNKNOWN;
            }
        }

        public int getValue() {
            return this.type;
        }
    }

    public AppSetupScreen(int hudRes, int hudResFail, int titleRes, int descriptionRes, int blueTextRes, int buttonRes, int titleFailRes, int descriptionFailRes, int buttonFailRes, ScreenType screenType, boolean isMandatory) {
        this.hudRes = hudRes;
        this.hudResFail = hudResFail;
        this.titleRes = titleRes;
        this.descriptionRes = descriptionRes;
        this.blueTextRes = blueTextRes;
        this.buttonRes = buttonRes;
        this.titleFailRes = titleFailRes;
        this.descriptionFailRes = descriptionFailRes;
        this.buttonFailRes = buttonFailRes;
        this.screenType = screenType;
        this.isMandatory = isMandatory;
    }

    public AppSetupScreen(int hudRes, int hudResFail, int titleRes, int descriptionRes, int buttonRes, int titleFailRes, int descriptionFailRes, int buttonFailRes, ScreenType screenType, boolean isMandatory) {
        this.hudRes = hudRes;
        this.hudResFail = hudResFail;
        this.titleRes = titleRes;
        this.descriptionRes = descriptionRes;
        this.buttonRes = buttonRes;
        this.titleFailRes = titleFailRes;
        this.descriptionFailRes = descriptionFailRes;
        this.buttonFailRes = buttonFailRes;
        this.screenType = screenType;
        this.isMandatory = isMandatory;
    }

    public AppSetupScreen(int hudRes, int titleRes, int descriptionRes, int buttonRes, ScreenType screenType, boolean isMandatory) {
        this.hudRes = hudRes;
        this.titleRes = titleRes;
        this.descriptionRes = descriptionRes;
        this.buttonRes = buttonRes;
        this.screenType = screenType;
        this.isMandatory = isMandatory;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.hudRes);
        dest.writeInt(this.hudResFail);
        dest.writeInt(this.titleRes);
        dest.writeInt(this.descriptionRes);
        dest.writeInt(this.blueTextRes);
        dest.writeInt(this.buttonRes);
        dest.writeInt(this.titleFailRes);
        dest.writeInt(this.descriptionFailRes);
        dest.writeInt(this.buttonFailRes);
        dest.writeInt(this.screenType.getValue());
        dest.writeInt(this.isMandatory ? 1 : 0);
    }

    protected AppSetupScreen(Parcel in) {
        this.hudRes = in.readInt();
        this.hudResFail = in.readInt();
        this.titleRes = in.readInt();
        this.descriptionRes = in.readInt();
        this.blueTextRes = in.readInt();
        this.buttonRes = in.readInt();
        this.titleFailRes = in.readInt();
        this.descriptionFailRes = in.readInt();
        this.buttonFailRes = in.readInt();
        this.screenType = ScreenType.fromValue(in.readInt());
        this.isMandatory = in.readInt() != 0;
    }
}
