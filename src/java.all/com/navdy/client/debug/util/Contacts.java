package com.navdy.client.debug.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.PhoneLookup;

public class Contacts {
    public static String lookupNameFromPhoneNumber(Context context, String phoneNumber) {
        Cursor c = null;
        try {
            Uri lookupUri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
            c = context.getContentResolver().query(lookupUri, new String[]{"display_name"}, null, null, null);
            if (c == null || c.getCount() <= 0 || !c.moveToNext()) {
                if (c != null) {
                    c.close();
                }
                return null;
            }
            String string = c.getString(c.getColumnIndexOrThrow("display_name"));
            if (c == null) {
                return string;
            }
            c.close();
            return string;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            return null;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            throw th;
        }
    }
}
