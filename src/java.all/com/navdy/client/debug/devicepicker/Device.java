package com.navdy.client.debug.devicepicker;

import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.connection.ConnectionInfo;

public class Device {
    protected ConnectionInfo[] connectionTypes;
    protected NavdyDeviceId deviceId;
    protected int preferredType;

    public static String prettyName(ConnectionInfo connection) {
        if (connection == null) {
            return "none";
        }
        String deviceDescription = connection.getDeviceId().getDeviceName();
        if (deviceDescription == null) {
            return connection.getAddress().getAddress();
        }
        return deviceDescription;
    }

    public Device(ConnectionInfo info) {
        this(info.getDeviceId());
        add(info);
    }

    public Device(NavdyDeviceId deviceId) {
        this.connectionTypes = new ConnectionInfo[PreferredConnectionType.values().length];
        this.preferredType = 0;
        this.deviceId = deviceId;
    }

    public void add(ConnectionInfo connectionInfo) {
        add(connectionInfo, true);
    }

    public void add(ConnectionInfo connectionInfo, boolean updateIfFound) {
        PreferredConnectionType type = PreferredConnectionType.fromConnectionType(connectionInfo.getType());
        if (updateIfFound || this.connectionTypes[type.getPriority()] == null) {
            this.connectionTypes[type.getPriority()] = connectionInfo;
        }
    }

    public NavdyDeviceId getDeviceId() {
        return this.deviceId;
    }

    public ConnectionInfo[] getConnectionTypes() {
        return (ConnectionInfo[]) this.connectionTypes.clone();
    }

    public ConnectionInfo getPreferredConnectionInfo() {
        ConnectionInfo preferredInfo = this.preferredType != -1 ? this.connectionTypes[this.preferredType] : null;
        if (preferredInfo != null) {
            return preferredInfo;
        }
        for (ConnectionInfo info : this.connectionTypes) {
            if (info != null) {
                return info;
            }
        }
        return null;
    }

    public int getPreferredType() {
        return this.preferredType;
    }

    public void setPreferredType(PreferredConnectionType type) {
        this.preferredType = type.getPriority();
    }

    public String getIpAddress() {
        ConnectionInfo tcpConnectionInfo = this.connectionTypes[PreferredConnectionType.TCP.getPriority()];
        if (tcpConnectionInfo != null) {
            return tcpConnectionInfo.getAddress().getAddress();
        }
        return null;
    }

    public String getBluetoothAddress() {
        ConnectionInfo btConnectionInfo = this.connectionTypes[PreferredConnectionType.BT.getPriority()];
        if (btConnectionInfo != null) {
            return btConnectionInfo.getAddress().getAddress();
        }
        return null;
    }

    public String getPrettyName() {
        String prettyName = this.deviceId.getDeviceName();
        if (prettyName != null && !this.deviceId.equals(NavdyDeviceId.UNKNOWN_ID)) {
            return prettyName;
        }
        prettyName = getIpAddress();
        if (prettyName != null) {
            return prettyName;
        }
        prettyName = getBluetoothAddress();
        if (prettyName != null) {
            return prettyName;
        }
        return this.deviceId.getDisplayName();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Device that = (Device) o;
        if (!this.deviceId.equals(that.deviceId)) {
            return false;
        }
        if (!this.deviceId.equals(NavdyDeviceId.UNKNOWN_ID)) {
            return true;
        }
        if (this.connectionTypes.length != that.connectionTypes.length) {
            return false;
        }
        for (int i = 0; i < this.connectionTypes.length; i++) {
            ConnectionInfo lhs = this.connectionTypes[i];
            ConnectionInfo rhs = that.connectionTypes[i];
            if (lhs == null || rhs == null) {
                if (lhs != rhs) {
                    return false;
                }
            } else if (!lhs.equals(rhs)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int result = this.deviceId.hashCode();
        if (this.deviceId.equals(NavdyDeviceId.UNKNOWN_ID)) {
            for (ConnectionInfo info : this.connectionTypes) {
                if (info != null) {
                    result = (result * 31) + info.hashCode();
                }
            }
        }
        return result;
    }

    public String toString() {
        return getPrettyName();
    }
}
