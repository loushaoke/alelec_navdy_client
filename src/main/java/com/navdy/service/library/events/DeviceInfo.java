//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.navdy.service.library.events;

import com.squareup.wire.Message;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import java.util.Collections;
import java.util.List;
import com.squareup.wire.ProtoEnum;

public final class DeviceInfo extends Message {
    public static final String DEFAULT_BUILDTYPE = "user";
    public static final String DEFAULT_CLIENTVERSION = "";
    public static final String DEFAULT_DEVICEID = "";
    public static final String DEFAULT_DEVICEMAKE = "unknown";
    public static final String DEFAULT_DEVICENAME = "";
    public static final String DEFAULT_DEVICEUUID = "";
    public static final Boolean DEFAULT_FORCEFULLUPDATE;
    public static final String DEFAULT_KERNELVERSION = "";
    public static final List<LegacyCapability> DEFAULT_LEGACYCAPABILITIES;
    public static final String DEFAULT_MODEL = "";
    public static final List<String> DEFAULT_MUSICPLAYERS_OBSOLETE;
    public static final DeviceInfo.Platform DEFAULT_PLATFORM;
    public static final String DEFAULT_PROTOCOLVERSION = "";
    public static final Integer DEFAULT_SYSTEMAPILEVEL = 0;
    public static final String DEFAULT_SYSTEMVERSION = "";
    private static final long serialVersionUID = 0L;

    @ProtoField(
        tag = 11,
        type = Datatype.STRING
    )
    public final String buildType;
    @ProtoField(
        tag = 16
    )
    public final Capabilities capabilities;
    @ProtoField(
        label = Label.REQUIRED,
        tag = 2,
        type = Datatype.STRING
    )
    public final String clientVersion;
    @ProtoField(
        label = Label.REQUIRED,
        tag = 1,
        type = Datatype.STRING
    )
    public final String deviceId;
    @ProtoField(
        tag = 12,
        type = Datatype.STRING
    )
    public final String deviceMake;
    @ProtoField(
        label = Label.REQUIRED,
        tag = 4,
        type = Datatype.STRING
    )
    public final String deviceName;
    @ProtoField(
        label = Label.REQUIRED,
        tag = 7,
        type = Datatype.STRING
    )
    public final String deviceUuid;
    @ProtoField(
        tag = 13,
        type = Datatype.BOOL
    )
    public final Boolean forceFullUpdate;
    @ProtoField(
        tag = 9,
        type = Datatype.STRING
    )
    public final String kernelVersion;
    @ProtoField(
        deprecated = true,
        enumType = LegacyCapability.class,
        label = Label.REPEATED,
        tag = 14,
        type = Datatype.ENUM
    )
    @Deprecated
    public final List<LegacyCapability> legacyCapabilities;
    @ProtoField(
        label = Label.REQUIRED,
        tag = 6,
        type = Datatype.STRING
    )
    public final String model;
    @ProtoField(
        deprecated = true,
        label = Label.REPEATED,
        tag = 15,
        type = Datatype.STRING
    )
    @Deprecated
    public final List<String> musicPlayers_OBSOLETE;
    @ProtoField(
        tag = 10,
        type = Datatype.ENUM
    )
    public final Platform platform;
    @ProtoField(
        label = Label.REQUIRED,
        tag = 3,
        type = Datatype.STRING
    )
    public final String protocolVersion;
    @ProtoField(
        tag = 8,
        type = Datatype.INT32
    )
    public final Integer systemApiLevel;
    @ProtoField(
        label = Label.REQUIRED,
        tag = 5,
        type = Datatype.STRING
    )
    public final String systemVersion;

    static {
        DEFAULT_PLATFORM = Platform.PLATFORM_iOS;
        DEFAULT_FORCEFULLUPDATE = false;
        DEFAULT_LEGACYCAPABILITIES = Collections.emptyList();
        DEFAULT_MUSICPLAYERS_OBSOLETE = Collections.emptyList();
    }

    public enum Platform implements ProtoEnum {
        PLATFORM_iOS(1),
        PLATFORM_Android(2);

        private final int value;

        private Platform(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    private DeviceInfo(DeviceInfo.Builder var1) {
        this(var1.deviceId, var1.clientVersion, var1.protocolVersion, var1.deviceName, var1.systemVersion, var1.model, var1.deviceUuid, var1.systemApiLevel, var1.kernelVersion, var1.platform, var1.buildType, var1.deviceMake, var1.forceFullUpdate, var1.legacyCapabilities, var1.musicPlayers_OBSOLETE, var1.capabilities);
        this.setBuilder(var1);
    }

    public DeviceInfo(String var1, String var2, String var3, String var4, String var5, String var6, String var7, Integer var8, String var9, Platform var10, String var11, String var12, Boolean var13, List<LegacyCapability> var14, List<String> var15, Capabilities var16) {
        this.deviceId = var1;
        this.clientVersion = var2;
        this.protocolVersion = var3;
        this.deviceName = var4;
        this.systemVersion = var5;
        this.model = var6;
        this.deviceUuid = var7;
        this.systemApiLevel = var8;
        this.kernelVersion = var9;
        this.platform = var10;
        this.buildType = var11;
        this.deviceMake = var12;
        this.forceFullUpdate = var13;
        this.legacyCapabilities = immutableCopyOf(var14);
        this.musicPlayers_OBSOLETE = immutableCopyOf(var15);
        this.capabilities = var16;
    }

    public boolean equals(Object var1) {
        boolean var2 = true;
        if (var1 != this) {
            if (!(var1 instanceof DeviceInfo)) {
                var2 = false;
            } else {
                DeviceInfo var3 = (DeviceInfo)var1;
                if (!this.equals(this.deviceId, var3.deviceId) || !this.equals(this.clientVersion, var3.clientVersion) || !this.equals(this.protocolVersion, var3.protocolVersion) || !this.equals(this.deviceName, var3.deviceName) || !this.equals(this.systemVersion, var3.systemVersion) || !this.equals(this.model, var3.model) || !this.equals(this.deviceUuid, var3.deviceUuid) || !this.equals(this.systemApiLevel, var3.systemApiLevel) || !this.equals(this.kernelVersion, var3.kernelVersion) || !this.equals(this.platform, var3.platform) || !this.equals(this.buildType, var3.buildType) || !this.equals(this.deviceMake, var3.deviceMake) || !this.equals(this.forceFullUpdate, var3.forceFullUpdate) || !this.equals(this.legacyCapabilities, var3.legacyCapabilities) || !this.equals(this.musicPlayers_OBSOLETE, var3.musicPlayers_OBSOLETE) || !this.equals(this.capabilities, var3.capabilities)) {
                    var2 = false;
                }
            }
        }

        return var2;
    }

    public int hashCode() {
        int var1 = 1;
        int var2 = 0;
        int var3 = this.hashCode;
        int var4 = var3;
        if (var3 == 0) {
            if (this.deviceId != null) {
                var4 = this.deviceId.hashCode();
            } else {
                var4 = 0;
            }

            if (this.clientVersion != null) {
                var3 = this.clientVersion.hashCode();
            } else {
                var3 = 0;
            }

            int var5;
            if (this.protocolVersion != null) {
                var5 = this.protocolVersion.hashCode();
            } else {
                var5 = 0;
            }

            int var6;
            if (this.deviceName != null) {
                var6 = this.deviceName.hashCode();
            } else {
                var6 = 0;
            }

            int var7;
            if (this.systemVersion != null) {
                var7 = this.systemVersion.hashCode();
            } else {
                var7 = 0;
            }

            int var8;
            if (this.model != null) {
                var8 = this.model.hashCode();
            } else {
                var8 = 0;
            }

            int var9;
            if (this.deviceUuid != null) {
                var9 = this.deviceUuid.hashCode();
            } else {
                var9 = 0;
            }

            int var10;
            if (this.systemApiLevel != null) {
                var10 = this.systemApiLevel.hashCode();
            } else {
                var10 = 0;
            }

            int var11;
            if (this.kernelVersion != null) {
                var11 = this.kernelVersion.hashCode();
            } else {
                var11 = 0;
            }

            int var12;
            if (this.platform != null) {
                var12 = this.platform.hashCode();
            } else {
                var12 = 0;
            }

            int var13;
            if (this.buildType != null) {
                var13 = this.buildType.hashCode();
            } else {
                var13 = 0;
            }

            int var14;
            if (this.deviceMake != null) {
                var14 = this.deviceMake.hashCode();
            } else {
                var14 = 0;
            }

            int var15;
            if (this.forceFullUpdate != null) {
                var15 = this.forceFullUpdate.hashCode();
            } else {
                var15 = 0;
            }

            int var16;
            if (this.legacyCapabilities != null) {
                var16 = this.legacyCapabilities.hashCode();
            } else {
                var16 = 1;
            }

            if (this.musicPlayers_OBSOLETE != null) {
                var1 = this.musicPlayers_OBSOLETE.hashCode();
            }

            if (this.capabilities != null) {
                var2 = this.capabilities.hashCode();
            }

            var4 = ((((((((((((((var4 * 37 + var3) * 37 + var5) * 37 + var6) * 37 + var7) * 37 + var8) * 37 + var9) * 37 + var10) * 37 + var11) * 37 + var12) * 37 + var13) * 37 + var14) * 37 + var15) * 37 + var16) * 37 + var1) * 37 + var2;
            this.hashCode = var4;
        }

        return var4;
    }

    public static final class Builder extends com.squareup.wire.Message.Builder<DeviceInfo> {
        public String buildType;
        public Capabilities capabilities;
        public String clientVersion;
        public String deviceId;
        public String deviceMake;
        public String deviceName;
        public String deviceUuid;
        public Boolean forceFullUpdate;
        public String kernelVersion;
        public List<LegacyCapability> legacyCapabilities;
        public String model;
        public List<String> musicPlayers_OBSOLETE;
        public Platform platform;
        public String protocolVersion;
        public Integer systemApiLevel;
        public String systemVersion;

        public Builder() {
        }

        public Builder(DeviceInfo var1) {
            super(var1);
            if (var1 != null) {
                this.deviceId = var1.deviceId;
                this.clientVersion = var1.clientVersion;
                this.protocolVersion = var1.protocolVersion;
                this.deviceName = var1.deviceName;
                this.systemVersion = var1.systemVersion;
                this.model = var1.model;
                this.deviceUuid = var1.deviceUuid;
                this.systemApiLevel = var1.systemApiLevel;
                this.kernelVersion = var1.kernelVersion;
                this.platform = var1.platform;
                this.buildType = var1.buildType;
                this.deviceMake = var1.deviceMake;
                this.forceFullUpdate = var1.forceFullUpdate;
                this.legacyCapabilities = DeviceInfo.copyOf(var1.legacyCapabilities);
                this.musicPlayers_OBSOLETE = DeviceInfo.copyOf(var1.musicPlayers_OBSOLETE);
                this.capabilities = var1.capabilities;
            }

        }

        public DeviceInfo build() {
            this.checkRequiredFields();
            return new DeviceInfo(this);
        }

        public DeviceInfo.Builder buildType(String var1) {
            this.buildType = var1;
            return this;
        }

        public DeviceInfo.Builder capabilities(Capabilities var1) {
            this.capabilities = var1;
            return this;
        }

        public DeviceInfo.Builder clientVersion(String var1) {
            this.clientVersion = var1;
            return this;
        }

        public DeviceInfo.Builder deviceId(String var1) {
            this.deviceId = var1;
            return this;
        }

        public DeviceInfo.Builder deviceMake(String var1) {
            this.deviceMake = var1;
            return this;
        }

        public DeviceInfo.Builder deviceName(String var1) {
            this.deviceName = var1;
            return this;
        }

        public DeviceInfo.Builder deviceUuid(String var1) {
            this.deviceUuid = var1;
            return this;
        }

        public DeviceInfo.Builder forceFullUpdate(Boolean var1) {
            this.forceFullUpdate = var1;
            return this;
        }

        public DeviceInfo.Builder kernelVersion(String var1) {
            this.kernelVersion = var1;
            return this;
        }

        @Deprecated
        public DeviceInfo.Builder legacyCapabilities(List<LegacyCapability> var1) {
            this.legacyCapabilities = checkForNulls(var1);
            return this;
        }

        public DeviceInfo.Builder model(String var1) {
            this.model = var1;
            return this;
        }

        @Deprecated
        public DeviceInfo.Builder musicPlayers_OBSOLETE(List<String> var1) {
            this.musicPlayers_OBSOLETE = checkForNulls(var1);
            return this;
        }

        public DeviceInfo.Builder platform(Platform var1) {
            this.platform = var1;
            return this;
        }

        public DeviceInfo.Builder protocolVersion(String var1) {
            this.protocolVersion = var1;
            return this;
        }

        public DeviceInfo.Builder systemApiLevel(Integer var1) {
            this.systemApiLevel = var1;
            return this;
        }

        public DeviceInfo.Builder systemVersion(String var1) {
            this.systemVersion = var1;
            return this;
        }
    }
}
