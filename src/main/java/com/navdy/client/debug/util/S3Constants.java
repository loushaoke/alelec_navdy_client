package com.navdy.client.debug.util;

import com.alelec.navdyclient.R;

public class S3Constants {
    public static final String BUCKET_NAME = "gesture-data";
    public static final String DB_FILE = "db.sqlite3";
    public static final String LOOKUP_FOLDER_PREFIX = "lookup";
    public static final String NAVDY_BUILDS_S3_BUCKET = "navdy-builds";
    public static final String NAVDY_PROD_RELEASE_S3_BUCKET = "navdy-prod-release";
    public static final String OTA_LOOKUP_FILE_NAME = "latest.json";
    public static final String OTA_RELEASE_NOTES_FILE_NAME_FORMAT = "release_notes_%s.json";
    public static final String S3_FILE_DELIMITER = "/";
    public static final String S3_OTA_BETA_PREFIX = "OTA/beta";
    public static final String S3_OTA_RELEASE_PREFIX = "OTA/release";
    public static final String S3_OTA_STABLE_PREFIX = "OTA";
    public static final String S3_OTA_UNSTABLE_PREFIX = "OTA/unstable";
    public static final String S3_PROTOCOL = "s3://";

    public enum BuildSource {
        STABLE(S3Constants.NAVDY_BUILDS_S3_BUCKET, S3Constants.S3_OTA_STABLE_PREFIX, BuildType.eng, R.string.build_source_stable),
        UNSTABLE(S3Constants.NAVDY_BUILDS_S3_BUCKET, S3Constants.S3_OTA_UNSTABLE_PREFIX, BuildType.eng, R.string.build_source_nightly),
        BETA(S3Constants.NAVDY_PROD_RELEASE_S3_BUCKET, S3Constants.S3_OTA_BETA_PREFIX, BuildType.user, R.string.build_source_beta),
        RELEASE(S3Constants.NAVDY_PROD_RELEASE_S3_BUCKET, S3Constants.S3_OTA_RELEASE_PREFIX, BuildType.user, R.string.build_source_release);
        
        private String bucket;
        private BuildType buildType;
        private int labelResourceId;
        private String prefix;

        public String getPrefix() {
            return this.prefix;
        }

        public String getBucket() {
            return this.bucket;
        }

        public BuildType getBuildType() {
            return this.buildType;
        }

        public int getLabelResourceId() {
            return this.labelResourceId;
        }

        private BuildSource(String bucket, String prefix, BuildType type, int resourceId) {
            this.prefix = prefix;
            this.bucket = bucket;
            this.buildType = type;
            this.labelResourceId = resourceId;
        }
    }

    public enum BuildType {
        user,
        eng
    }

    public static BuildSource[] getSourcesForBuildType(BuildType type) {
        switch (type) {
            case user:
                return new BuildSource[]{BuildSource.BETA, BuildSource.RELEASE};
            case eng:
                return new BuildSource[]{BuildSource.UNSTABLE, BuildSource.STABLE};
            default:
                return null;
        }
    }

    public static BuildSource getDefaultSourceForBuildType(BuildType buildType) {
        return getSourcesForBuildType(buildType)[1];
    }
}
