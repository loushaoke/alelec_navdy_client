package com.navdy.client.debug.devicepicker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Checkable;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.alelec.navdyclient.R.id;
import com.navdy.service.library.device.connection.ConnectionInfo;
import java.util.List;
import java.util.Locale;

public class DeviceAdapter extends ArrayAdapter<Device> {
    static int[] button_ids;
    static PreferredConnectionType[] types = PreferredConnectionType.values();

    private static class ViewHolder {
        private OnClickListener clickListener;
        private Button[] connectionTypes;
        private Device device;
        private TextView key;

        private ViewHolder(View view) {
            this.connectionTypes = new Button[3];
            this.clickListener = new OnClickListener() {
                public void onClick(View v) {
                    ViewHolder.this.device.setPreferredType((PreferredConnectionType) v.getTag());
                }
            };
            this.key = (TextView) view.findViewById(R.id.key);
            if (DeviceAdapter.button_ids == null) {
                DeviceAdapter.lookupIds();
            }
            for (int i = 0; i < DeviceAdapter.button_ids.length; i++) {
                Button button = (Button) view.findViewById(DeviceAdapter.button_ids[i]);
                button.setOnClickListener(this.clickListener);
                button.setTag(DeviceAdapter.types[i]);
                this.connectionTypes[i] = button;
            }
        }

        private void setDevice(Device device) {
            this.device = device;
            this.key.setText(device.toString());
            ConnectionInfo[] connectionTypes = device.getConnectionTypes();
            for (int i = 0; i < connectionTypes.length; i++) {
                boolean z;
                Button button = this.connectionTypes[i];
                button.setVisibility(connectionTypes[i] != null ? 0 : 8);
                Checkable checkable = (Checkable) button;
                if (i == device.getPreferredType()) {
                    z = true;
                } else {
                    z = false;
                }
                checkable.setChecked(z);
            }
        }
    }

    static void lookupIds() {
        Class clazz = id.class;
        button_ids = new int[types.length];
        for (PreferredConnectionType type : types) {
            try {
                button_ids[type.getPriority()] = clazz.getField("connection_type_" + type.name().toLowerCase(Locale.US)).getInt(null);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            }
        }
    }

    public DeviceAdapter(Context context, List<Device> objects) {
        super(context, R.layout.list_item_device, objects);
    }

    public View getView(int pos, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_device, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.setDevice((Device) getItem(pos));
        return convertView;
    }
}
