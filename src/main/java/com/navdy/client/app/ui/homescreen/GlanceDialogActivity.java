package com.navdy.client.app.ui.homescreen;

import android.os.Bundle;
import android.view.View;
import com.alelec.navdyclient.R;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.glances.GlanceUtils;

public class GlanceDialogActivity extends BaseActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.dialog_glance_demo);
    }

    public void sendATestGlance(View v) {
        GlanceUtils.sendTestGlance(this, this.logger);
    }

    public void onCloseClick(View view) {
        onBackPressed();
    }
}
