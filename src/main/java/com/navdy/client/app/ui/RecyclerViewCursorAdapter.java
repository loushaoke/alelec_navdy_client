package com.navdy.client.app.ui;

import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;

public abstract class RecyclerViewCursorAdapter<VH extends ViewHolder> extends Adapter<VH> {
    private static final int FOOTER_VIEW = -2;
    private static final int HEADER_VIEW = -1;
    private static final Logger logger = new Logger(RecyclerViewCursorAdapter.class);
    protected Context context;
    protected Cursor cursor = null;
    private CursorAdapter cursorAdapter;
    protected boolean dataIsValid = false;
    private DataSetObserver dataSetObserver;
    protected boolean hasFooter = false;
    protected boolean hasHeader = false;
    private boolean observerRegistered;

    public abstract long getItemId(int i);

    public abstract Cursor getNewCursor();

    public abstract void onBindViewHolder(VH vh, int i);

    public abstract VH onCreateFooterViewHolder(ViewGroup viewGroup);

    public abstract VH onCreateHeaderViewHolder(ViewGroup viewGroup);

    public abstract VH onCreateNormalViewHolder(ViewGroup viewGroup);

    public RecyclerViewCursorAdapter(Context context, boolean hasHeader, boolean hasFooter) {
        this.context = context;
        this.hasHeader = hasHeader;
        this.hasFooter = hasFooter;
        setHasStableIds(true);
        this.dataSetObserver = new DataSetObserver() {
            public void onChanged() {
                super.onChanged();
                RecyclerViewCursorAdapter.this.dataIsValid = true;
                RecyclerViewCursorAdapter.this.changeCursor(RecyclerViewCursorAdapter.this.getNewCursor());
            }

            public void onInvalidated() {
                super.onInvalidated();
                RecyclerViewCursorAdapter.this.dataIsValid = false;
                RecyclerViewCursorAdapter.this.changeCursor(RecyclerViewCursorAdapter.this.getNewCursor());
            }
        };
    }

    public void notifyDbChanged() {
        this.dataSetObserver.onChanged();
    }

    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        changeCursor(getNewCursor());
    }

    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        if (this.cursor != null) {
            if (this.observerRegistered) {
                this.cursor.unregisterDataSetObserver(this.dataSetObserver);
                this.observerRegistered = false;
                logger.v("observer unregistered detached");
            }
            IOUtils.closeStream(this.cursor);
            this.cursor = null;
        }
    }

    public int getItemCount() {
        int count = 0;
        if (this.cursor != null) {
            count = this.cursor.getCount();
            if (count > 0 && this.hasFooter) {
                count++;
            }
        }
        return this.hasHeader ? count + 1 : count;
    }

    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == -1) {
            return onCreateHeaderViewHolder(parent);
        }
        if (viewType == -2) {
            return onCreateFooterViewHolder(parent);
        }
        return onCreateNormalViewHolder(parent);
    }

    public int getItemViewType(int position) {
        if (this.hasHeader && position == 0) {
            return -1;
        }
        if (this.hasFooter && position == this.cursor.getCount()) {
            return -2;
        }
        return super.getItemViewType(position);
    }

    public void changeCursor(Cursor newCursor) {
        IOUtils.closeStream(swapCursor(newCursor));
    }

    private Cursor swapCursor(Cursor newCursor) {
        boolean z = false;
        if (newCursor == this.cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        if (oldCursor != null && this.observerRegistered) {
            oldCursor.unregisterDataSetObserver(this.dataSetObserver);
            logger.v("observer unregistered old");
            this.observerRegistered = false;
        }
        this.cursor = newCursor;
        if (this.cursor != null) {
            z = true;
        }
        this.dataIsValid = z;
        if (this.cursor != null) {
            logger.v("observer registered");
            this.cursor.registerDataSetObserver(this.dataSetObserver);
            this.observerRegistered = true;
        }
        notifyDataSetChanged();
        if (this.cursorAdapter == null) {
            this.cursorAdapter = new CursorAdapter(this.context, this.cursor, true) {
                public View newView(Context context, Cursor cursor, ViewGroup parent) {
                    return null;
                }

                public void bindView(View view, Context context, Cursor cursor) {
                }
            };
            return oldCursor;
        }
        this.cursorAdapter.swapCursor(this.cursor);
        return oldCursor;
    }

    public void close() {
        logger.v("close");
        try {
            if (this.cursor != null) {
                if (this.observerRegistered) {
                    this.observerRegistered = false;
                    this.cursor.unregisterDataSetObserver(this.dataSetObserver);
                    logger.v("observer unregistered");
                }
                IOUtils.closeStream(this.cursor);
            }
        } catch (Throwable t) {
            logger.e(t);
        }
    }
}
