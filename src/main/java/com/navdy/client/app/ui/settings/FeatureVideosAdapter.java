package com.navdy.client.app.ui.settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.ImageCache;
import com.navdy.client.app.framework.util.ImageUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

class FeatureVideosAdapter extends Adapter {
    private static final int CACHE_SIZE = 52428800;
    private static final int IMAGE_MAX_SIZE = 4194304;
    private Context context = NavdyApplication.getAppContext();
    private final FeatureVideo[] featureVideoArray = new FeatureVideo[]{new FeatureVideo("image_video_features_overview", this.context.getString(R.string.features_overview), R.drawable.image_video_features_overview, "1:59", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/00-FeatureOverview-Android-App.mp4"), new FeatureVideo("image_video_power", this.context.getString(R.string.features_power), R.drawable.image_video_power, "0:30", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/01-PowerAndYourNavdyDisplay-Android-App.mp4"), new FeatureVideo("image_video_connectiing", this.context.getString(R.string.features_connection), R.drawable.image_video_connectiing, "0:53", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/02-ConnectingtoYourNavdyDisplay-Android-App.mp4"), new FeatureVideo("image_video_mobile_app_overview", this.context.getString(R.string.features_mobile_overview), R.drawable.image_video_mobile_app_overview, "0:53", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/03-MobileAppOverview-Android-App.mp4"), new FeatureVideo("image_video_dial", this.context.getString(R.string.features_dial), R.drawable.image_video_dial, "0:47", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/04-UsingYourNavdyDial-Android-App.mp4"), new FeatureVideo("image_gestures", this.context.getString(R.string.features_gestures), R.drawable.image_gestures, "1:03", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/05-UsingGestures-Android-App.mp4"), new FeatureVideo("image_video_brightness", this.context.getString(R.string.features_brightness), R.drawable.image_video_brightness, "0:31", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/06-AdjustingBrightness-Android-App.mp4"), new FeatureVideo("image_video_dash_mode", this.context.getString(R.string.features_dash_mode), R.drawable.image_video_dash_mode, "1:32", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/07-DashMode-Android-App.mp4"), new FeatureVideo("image_video_map_mode", this.context.getString(R.string.features_map_mode), R.drawable.image_video_map_mode, "1:24", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/08-MapMode-Android-App.mp4"), new FeatureVideo("image_video_searching", this.context.getString(R.string.features_search), R.drawable.image_video_searching, "1:14", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/09-SearchingForANewDestination-Android-App.mp4"), new FeatureVideo("image_video_adding_favorites", this.context.getString(R.string.features_favorites), R.drawable.image_video_adding_favorites, "0:46", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/10-AddingFavoritePlaces-Android-App.mp4"), new FeatureVideo("image_video_driving_favorite", this.context.getString(R.string.features_favorites_navigation), R.drawable.image_video_driving_favorite, "0:27", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/11-DrivingToAFavPlace-Android-App.mp4"), new FeatureVideo("image_video_driving_suggested", this.context.getString(R.string.features_suggested_places), R.drawable.image_video_driving_suggested, "0:30", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/12-DrivingToASuggestedPlace-Android-App.mp4"), new FeatureVideo("image_video_answering_calls", this.context.getString(R.string.features_answering_calls), R.drawable.image_video_answering_calls, "0:29", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/13-AnsweringCalls-Android-App.mp4"), new FeatureVideo("image_video_making_calls", this.context.getString(R.string.features_making_calls), R.drawable.image_video_making_calls, "0:38", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/14-MakingCalls-Android-App.mp4"), new FeatureVideo("image_video_reading_glances", this.context.getString(R.string.features_reading_glances), R.drawable.image_video_reading_glances, "1:43", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/15-ReadingGlancesFromYourPhone-Android-App.mp4"), new FeatureVideo("image_video_music", this.context.getString(R.string.features_controlling_music), R.drawable.image_video_music, "0:43", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/16-ControllingYourMusic-Android-App.mp4"), new FeatureVideo("image_video_google_now", this.context.getString(R.string.features_using_google_now), R.drawable.image_video_google_now, "0:55", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/17-UsingGoogleNow-Android-App.mp4"), new FeatureVideo("image_video_up_to_date", this.context.getString(R.string.feature_updating_navdy), R.drawable.image_video_up_to_date, "0:38", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/18-KeepingNavdyUpToDate-Android-App.mp4")};
    private ArrayList<FeatureVideo> featureVideoArrayList = new ArrayList();
    private HashSet<String> hasWatched = new HashSet();
    private ImageCache imageCache;
    private Logger logger = new Logger(FeatureVideosAdapter.class);

    public static class FeatureVideo {
        public String duration;
        public int imageResource;
        public String link;
        public String sharedPrefKey;
        public String title;

        public FeatureVideo(String sharedPrefKey, String title, int imageResource, String duration, String link) {
            this.sharedPrefKey = sharedPrefKey;
            this.title = title;
            this.imageResource = imageResource;
            this.duration = duration;
            this.link = link;
        }
    }

    FeatureVideosAdapter(Context activityContext) {
        this.context = activityContext;
        this.featureVideoArrayList = new ArrayList(Arrays.asList(this.featureVideoArray));
        this.imageCache = new ImageCache(CACHE_SIZE, 4194304);
        initImageCache();
        initHasWatched();
    }

    public FeatureVideo getItem(int position) {
        return (FeatureVideo) this.featureVideoArrayList.get(position);
    }

    public int getItemCount() {
        return this.featureVideoArrayList.size();
    }

    public FeatureVideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FeatureVideoViewHolder(LayoutInflater.from(this.context).inflate(R.layout.feature_video_layout, parent, false));
    }

    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final FeatureVideo featureVideo = (FeatureVideo) this.featureVideoArrayList.get(position);
        if (viewHolder instanceof FeatureVideoViewHolder) {
            FeatureVideoViewHolder featureVideoViewHolder = (FeatureVideoViewHolder) viewHolder;
            if (featureVideo != null) {
                featureVideoViewHolder.title.setText(featureVideo.title);
                Bitmap bitmap = this.imageCache.getBitmap(featureVideo.title);
                if (bitmap != null) {
                    featureVideoViewHolder.image.setImageBitmap(bitmap);
                } else {
                    featureVideoViewHolder.image.setImageResource(featureVideo.imageResource);
                }
                featureVideoViewHolder.duration.setText(featureVideo.duration);
                if (this.hasWatched.contains(featureVideo.sharedPrefKey)) {
                    featureVideoViewHolder.watchedLayout.setVisibility(VISIBLE);
                } else {
                    featureVideoViewHolder.watchedLayout.setVisibility(INVISIBLE);
                }
                featureVideoViewHolder.row.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        FeatureVideosAdapter.this.saveVideoAsWatched(featureVideo.sharedPrefKey);
                        Intent intent = new Intent(FeatureVideosAdapter.this.context, VideoPlayerActivity.class);
                        intent.putExtra(VideoPlayerActivity.EXTRA_VIDEO_URL, featureVideo.link);
                        FeatureVideosAdapter.this.context.startActivity(intent);
                    }
                });
                return;
            }
            return;
        }
        this.logger.e("The ViewHolder was not a FeatureVideoViewHolder.");
    }

    private void initImageCache() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                int width = FeatureVideosAdapter.this.context.getResources().getDimensionPixelSize(R.dimen.feature_video_width);
                int height = FeatureVideosAdapter.this.context.getResources().getDimensionPixelSize(R.dimen.feature_video_height);
                Iterator it = FeatureVideosAdapter.this.featureVideoArrayList.iterator();
                while (it.hasNext()) {
                    FeatureVideo featureVideo = (FeatureVideo) it.next();
                    Bitmap bitmap = ImageUtils.getBitmap(featureVideo.imageResource, width, height);
                    FeatureVideosAdapter.this.logger.d("bitmap size: " + ImageCache.getBitmapSize(bitmap));
                    FeatureVideosAdapter.this.imageCache.putBitmap(featureVideo.title, bitmap);
                }
            }
        }, 1);
    }

    private void initHasWatched() {
        SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();
        Iterator it = this.featureVideoArrayList.iterator();
        while (it.hasNext()) {
            FeatureVideo featureVideo = (FeatureVideo) it.next();
            if (sharedPreferences.getBoolean(featureVideo.sharedPrefKey, false)) {
                this.hasWatched.add(featureVideo.sharedPrefKey);
            }
        }
    }

    private void saveVideoAsWatched(String featureVideoKey) {
        if (StringUtils.isEmptyAfterTrim(featureVideoKey)) {
            this.logger.e("featureVideoKey was empty");
            return;
        }
        this.hasWatched.add(featureVideoKey);
        this.logger.d("hasWatched: " + featureVideoKey);
        SettingsUtils.getSharedPreferences().edit().putBoolean(featureVideoKey, true).apply();
    }
}
