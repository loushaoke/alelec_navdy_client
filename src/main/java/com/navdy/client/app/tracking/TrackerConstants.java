package com.navdy.client.app.tracking;

import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Suggestion;
import com.navdy.client.app.framework.models.Suggestion.SuggestionType;
import com.navdy.service.library.log.Logger;

public class TrackerConstants {
    public static final Logger logger = new Logger(TrackerConstants.class);

    public static class Event {
        public static final String BATTERY_LOW = "Battery_Low";
        public static final String CAR_INFO_CHANGED = "Car_Info_Changed";
        public static final String DELETING_A_FAVORITE = "Deleting_A_Favorite";
        public static final String GLANCES_CONFIGURED = "Glances_Configured";
        public static final String HAS_KILLED = "MapEngine_Forced_Crash";
        public static final String HAS_PHOENIX = "MapEngine_Forced_Restart";
        public static final String HUD_CONNECTION_ESTABLISHED = "Hud_Connection_Established";
        public static final String HUD_CONNECTION_LOST = "Hud_Connection_Lost";
        public static final String HUD_VOICE_SEARCH_AMBIENT_NOISE_FAILURE = "Hud_Voice_Search_ambient_noise_failure";
        public static final String MUSIC_PLAYLIST_COMPLETE_REINDEX = "Music_Playlist_Complete_Reindex";
        public static final String NAVIGATE_USING_FAVORITES = "Navigate_Using_Favorites";
        public static final String NAVIGATE_USING_GOOGLE_NOW = "Navigate_Using_Google_Now";
        public static final String NAVIGATE_USING_HUD_VOICE_SEARCH = "Navigate_Using_Hud_Voice_Search";
        public static final String NAVIGATE_USING_SEARCH_RESULTS = "Navigate_Using_Search_Results";
        public static final String NO_END_POINT = "No_End_Point";
        public static final String PAIR_PHONE_TO_DISPLAY = "PairPhoneToDisplay";
        public static final String PERMISSION_REJECTED = "Permission_Rejected";
        public static final String SAVING_A_FAVORITE = "Saving_A_Favorite";
        public static final String SCREEN_VIEWED = "Screen_Viewed";
        public static final String SEARCH_FOR_LOCATION = "Search_For_Location";
        public static final String SEARCH_USING_DESTINATION_FINDER = "Search_Using_Destination_Finder";
        public static final String SEARCH_USING_GOOGLE_NOW = "Search_Using_Google_Now";
        public static final String SET_DESTINATION = "Set_Destination";
        public static final String SET_HOME = "Set_Home";
        public static final String SET_WORK = "Set_Work";
        public static final String SUPPORT_TICKET_CREATED = "Support_Ticket_Created";
        public static final String SWITCHED_TO_DEFAULT = "Switched_to_default";
        public static final String SWITCHED_TO_PLACE_SEARCH = "Switched_to_place_search";

        public static class Audio {
            public static final String TTS_HFP_FAILURE = "TTS_HFP_FAILURE";
        }

        public static class Debug {
            public static final String HERE_GEOCODE_COMPLETED_AFTER_TIMEOUT = "Debug_Here_Geocode_Completed_After_Timeout";
            public static final String HERE_GEOCODE_TIMEOUT = "Debug_Here_Geocode_Timeout";
        }

        public static class Install {
            public static final String ACTUALLY_I_ALREADY_HAVE_MED_TALL_MOUNT = "Actually_I_Already_Have_Med_Tall_Mount";
            public static final String CONFIGURATION_AT_COMPLETION = "Configuration_At_Completion";
            public static final String CONTINUE_WITH_UNRECOMMENDED_SHORT_MOUNT = "Continue_With_Unrecommended_Short_Mount";
            public static final String CONTINUE_WITH_UNSUPPORTED_SHORT_MOUNT = "Continue_With_Unsupported_Short_Mount";
            public static final String FIRST_LAUNCH_COMPLETED = "First_Launch_Completed";
            public static final String MEDIUM_TALL_MOUNT_NOT_RECOMMENDED_CONTINUE = "Medium_Tall_Mount_Not_Recommended_Continue";
            public static final String MOUNT_PICKER_SELECTION = "Mount_Picker_Selection";
            public static final String PURCHASE_MOUNT_KIT = "Purchase_Mount_Kit";
            public static final String SHORT_MOUNT_NOT_RECOMMENDED_CONTINUE = "Short_Mount_Not_Recommended_Continue";
            public static final String USER_PICKED_MEDIUM_MOUNT = "User_Picked_Medium_Mount";
            public static final String USER_PICKED_SHORT_MOUNT = "User_Picked_Short_Mount";
            public static final String USER_PICKED_TALL_MOUNT = "User_Picked_Tall_Mount";
            public static final String VIDEO_TAPPED_ON_INSTALL_COMPLETE = "Video_Tapped_On_Install_Complete";
        }

        public static class Settings {
            public static final String AUDIO_SETTINGS_CHANGED = "Audio_Settings_Changed";
            public static final String DIAL_SETTINGS_CHANGED = "Dial_Settings_Changed";
            public static final String NAVIGATION_SETTINGS_CHANGED = "Navigation_Settings_Changed";
            public static final String OTA_SETTINGS_CHANGED = "Ota_Settings_Changed";
            public static final String PROFILE_SETTINGS_CHANGED = "Profile_Settings_Changed";
        }
    }

    public static class Attributes {
        public static final String CAR_MAKE = "Car_Make";
        public static final String CAR_MANUAL_ENTRY = "Vehicle_Data_Entry_Manual";
        public static final String CAR_MODEL = "Car_Model";
        public static final String CAR_YEAR = "Car_Year";
        public static final String CONTACTS = "Contacts";
        static final String CURRENT_SCREEN = "Current_Screen";
        public static final String DURING_FLE = "During_Fle";
        public static final String FIRST_LAUNCH_ATTRIBUTE = "First_Launch";
        public static final String HUD_SERIAL_NUMBER = "Hud_Serial_Number";
        public static final String HUD_VERSION_NUMBER = "Hud_Version_Number";
        public static final String LOCATION = "Location";
        public static final String MICROPHONE = "Microphone";
        public static final String MOBILE_APP_VERSION = "Mobile_App_Version";
        public static final String PHONE = "Phone";
        public static final String SMS = "Sms";
        public static final String STORAGE = "Storage";
        public static final String SUCCESS_ATTRIBUTE = "Success";
        public static final String TYPE = "Type";
        public static final String VIN = "VIN";

        public static class DestinationAttributes {
            public static final String FAVORITE_TYPE = "Favorite_Type";
            public static final String NEXT_TRIP = "Next_Trip";
            public static final String ONLINE = "Online";
            public static final String SOURCE = "Source";
            public static final String TYPE = "Type";

            public static class FAVORITE_TYPE_VALUES {
                private static final String CONTACT = "Contact";
                private static final String HOME = "Home";
                private static final String NONE = "None";
                private static final String OTHER = "Other";
                private static final String WORK = "Work";

                public static String getFavoriteTypeValue(Destination destination) {
                    if (destination.favoriteType == -3) {
                        return HOME;
                    }
                    if (destination.favoriteType == -2) {
                        return WORK;
                    }
                    if (destination.favoriteType == -4) {
                        return CONTACT;
                    }
                    if (destination.isFavoriteDestination()) {
                        return OTHER;
                    }
                    return "None";
                }
            }

            public static class SOURCE_VALUES {
                public static final String DETAILS = "Details";
                public static final String DROP_PIN = "Drop_Pin";
                public static final String FAVORITES_LIST = "Favorites_List";
                public static final String SEARCH = "Search";
                public static final String SUGGESTION_LIST = "Suggestion_List";
                public static final String THIRD_PARTY_INTENT = "Third_Party_Intent";
            }

            public static class TYPE_VALUES {
                public static final String CALENDAR = "Calendar";
                public static final String DROP_PIN = "Drop_Pin";
                public static final String FAVORITE = "Favorite";
                public static final String INTENT = "Intent";
                public static final String RECENT = "Recent";
                public static final String RECOMMENDATION = "Recommendation";
                public static final String SEARCH_AUTOCOMPLETE = "Search_Autocomplete";
                public static final String SEARCH_CONTACT = "Search_Contact";
                public static final String SEARCH_MAP = "Search_Map";
                public static final String SEARCH_QUICK = "Search_Quick";
                public static final String SEARCH_RESULT = "Search_Result";
                public static final String UNKNOWN = "Unknown";

                public static String getSetDestinationTypeFromSuggestion(Suggestion suggestion) {
                    if (suggestion == null) {
                        return UNKNOWN;
                    }
                    if (suggestion.isRecommendation()) {
                        return RECOMMENDATION;
                    }
                    if (suggestion.getType() == SuggestionType.CALENDAR) {
                        return CALENDAR;
                    }
                    return RECENT;
                }
            }
        }

        public static class HudVoiceSearchAttributes {
            public static final String NOISE_RETRY = "Noise_Retry";
        }

        public static class InstallAttributes {
            public static final String MEDIUM_MOUNT = "Medium_Mount";
            public static final String MOUNT_PICKER_MOUNT_TYPE = "Mount_Type";
            public static final String MOUNT_PICKER_MOUNT_TYPE_MED_TALL = "Medium_Tall";
            public static final String MOUNT_PICKER_MOUNT_TYPE_SHORT = "Short";
            public static final String MOUNT_PICKER_USE_CASE = "Use_Case";
            public static final String NB_CONFIGURATIONS = "Nb_Configurations";
            public static final String NEW_BOX = "New_Box";
            public static final String NEW_BOX_PLUS_MOUNTS = "New_Box_Plus_Mounts";
            public static final String OLD_BOX = "Old_Box";
            public static final String SELECTED_BOX = "Selected_Box";
            public static final String SELECTED_MOUNT = "Selected_Mount";
            public static final String SHORT_MOUNT = "Short_Mount";
            public static final String TALL_MOUNT = "Tall_Mount";
            public static final String VIDEO_URL = "Video_URL";
        }

        public static class MusicAttributes {
            public static final String NUM_GPM_PLAYLISTS_INDEXED = "Num_Gpm_Playlists_Indexed";
            public static final String NUM_GPM_TRACKS_INDEXED = "Num_Gpm_Tracks_Indexed";
        }
    }

    public static class Screen {
        public static final String ACTIVE_TRIP = "Active_Trip";
        public static final String DETAILS = "Destination_Details";
        public static final String EDIT_FAVORITES = "Edit_Favorites";
        public static final String NOTIFICATION_GLANCES = "Notification_Glances";
        public static final String ROUTING = "Routing";
        public static final String SEARCH = "Search";
        public static final String SPLASH = "Splash";

        public static class FirstLaunch {
            public static final String CAR_INFO = "First_Launch_Car_Info";
            public static final String EDIT_CAR_INFO = "First_Launch_Edit_Car_Info";
            public static final String FIRST_LAUNCH = "First_Launch";
            public static final String INSTALL_VIDEO = "First_Launch_Install_Video";
            public static final String SCREEN_LIGHT = "First_Launch_Screen_Light";

            public static class Install {
                public static final String BOX_PICKER = "Installation_Box_Picker";
                public static final String BOX_VIEWER_NEW_BOX = "Installation_Box_Viewer_New_Box";
                public static final String BOX_VIEWER_NEW_BOX_PLUS_MOUNTS = "Installation_Box_Viewer_New_Box_Plus_Mounts";
                public static final String BOX_VIEWER_OLD_BOX = "Installation_Box_Viewer_Old_Box";
                public static final String CABLE_PICKER = "Installation_Plugging_In";
                public static final String DIAL = "Installation_Dial";
                public static final String FINISHED = "Installation_Finished";
                public static final String LENS_POSITION = "Installation_Lens_Position";
                public static final String MEDIUM_MOUNT_RECOMMENDED = "Installation_Mount_Picker_Medium_Mount_Recommended";
                public static final String MEDIUM_TALL_IS_TOO_HIGH = "Installation_Mount_Picker_Medium_Tall_Is_Too_High";
                public static final String MEDIUM_TALL_MOUNT = "Installation_Medium_Tall_Mount";
                public static final String MEDIUM_TALL_MOUNT_NOT_RECOMMENDED = "Installation_Medium_Tall_Mount_Not_Recommended";
                public static final String MOUNT_KIT_NEEDED = "Installation_Mount_Kit_Needed";
                public static final String MOUNT_KIT_PURCHASE = "Installation_Mount_Kit_Purchase";
                public static final String NO_MOUNT_SUPPORTED = "Installation_Mount_Picker_No_Mount_Supported";
                public static final String OVERVIEW = "Installation_Overview";
                public static final String PLUG_CLA = "Installation_Plug_CLA";
                public static final String PLUG_OBD = "Installation_Plug_OBD";
                public static final String POWER_ON = "Installation_Power_On";
                public static final String READY_CHECK = "Installation_Ready_Check";
                public static final String SECURE = "Installation_Secure";
                public static final String SHORT_IS_TOO_LOW = "Installation_Mount_Picker_Short_Is_Too_Low";
                public static final String SHORT_MOUNT = "Installation_Short_Mount";
                public static final String SHORT_MOUNT_NOT_RECOMMENDED = "Installation_Short_Mount_Not_Recommended";
                public static final String TIDY_UP = "Installation_Tidy_Up";
            }

            public static class Marketing {
                public static final String EFFORTLESS_CONTROL = "Effortless_Control";
                public static final String GET_STARTED = "Get_Started";
                public static final String MEET_NAVDY = "Meet_Navdy";
                public static final String NEVER_MISS_A_TURN = "Never_Miss_A_Turn";
                public static final String STAY_CONNECTED = "Stay_Connected";
            }
        }

        public static class Home {
            public static final String FAVORITES = "Home_Favorites";
            public static final String GLANCES = "Home_Glances";
            public static final String HOME = "Home_Start";
            public static final String TRIPS = "Home_Trips";

            public static String tag(int index) {
                return new String[]{HOME, FAVORITES, GLANCES, TRIPS}[index];
            }
        }

        public static class Settings {
            public static final String AUDIO = "Settings_Audio";
            public static final String BLUETOOTH = "Settings_Bluetooth_Pairing";
            public static final String CONTACT = "Settings_Contact_Us";
            public static final String DEBUG = "Settings_Debug";
            public static final String LEGAL = "Settings_Legal";
            public static final String MESSAGING = "Settings_Messaging";
            public static final String NAVIGATION = "Settings_Navigation";
            public static final String PROFILE = "Settings_Profile";
            public static final String SUPPORT = "Settings_Support";
            public static final String UPDATE = "Settings_Update";
        }

        public static class Web {
            public static String tag(String type) {
                return "Legal_" + type;
            }
        }
    }
}
