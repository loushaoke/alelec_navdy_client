package com.navdy.client.app.framework.service;

import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.provider.ContactsContract.Contacts;
import com.navdy.client.app.framework.servicehandler.ContactServiceHandler;
import com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler;
import com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler;
import com.navdy.client.app.framework.servicehandler.MessageServiceHandler;
import com.navdy.client.app.framework.servicehandler.MusicServiceHandler;
import com.navdy.client.app.framework.servicehandler.PhotoServiceHandler;
import com.navdy.client.app.framework.servicehandler.PlaceTypeSearchServiceHandler;
import com.navdy.client.app.framework.servicehandler.SettingsServiceHandler;
import com.navdy.client.app.framework.servicehandler.SpeechServiceHandler;
import com.navdy.client.app.framework.servicehandler.TelephonyServiceHandler;
import com.navdy.client.app.framework.servicehandler.TripUpdateServiceHandler;
import com.navdy.client.app.framework.servicehandler.VoiceServiceHandler;
import com.navdy.client.app.framework.util.ContactObserver;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.proxy.ProxyListener;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.device.connection.AcceptorListener;
import com.navdy.service.library.device.connection.ConnectionListener;
import com.navdy.service.library.device.connection.ConnectionService;
import com.navdy.service.library.device.connection.ConnectionType;
import com.navdy.service.library.device.connection.ProxyService;
import com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster;
import com.navdy.service.library.network.BTSocketAcceptor;
import com.navdy.service.library.util.Listenable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ClientConnectionService extends ConnectionService {
    private final IBinder binder = new LocalBinder();
    private DeviceChangeBroadcaster deviceChangeBroadcaster = new DeviceChangeBroadcaster();
    private List<Object> serviceHandlers;

    public static class DeviceChangeBroadcaster extends Listenable<DeviceChangeBroadcaster.Listener> {

        public interface Listener extends com.navdy.service.library.util.Listenable.Listener {
            void onDeviceChanged(RemoteDevice remoteDevice);
        }

        interface EventDispatcher extends Listenable.EventDispatcher<DeviceChangeBroadcaster, ClientConnectionService.DeviceChangeBroadcaster.Listener> {
        }

        void dispatchDeviceChangedEvent(final RemoteDevice newDevice) {
            dispatchToListeners(new EventDispatcher() {
                public void dispatchEvent(DeviceChangeBroadcaster source, Listener listener) {
                    listener.onDeviceChanged(newDevice);
                }
            });
        }
    }

    public class LocalBinder extends Binder {
        public ClientConnectionService getService() {
            return ClientConnectionService.this;
        }
    }

    public void addListener(DeviceChangeBroadcaster.Listener listener) {
        this.deviceChangeBroadcaster.addListener(listener);
    }

    public void removeListener(DeviceChangeBroadcaster.Listener listener) {
        this.deviceChangeBroadcaster.removeListener(listener);
    }

    public void onCreate() {
        super.onCreate();
        ContactServiceHandler contactServiceHandler = ContactServiceHandler.getInstance();
        this.serviceHandlers = new ArrayList();
        this.serviceHandlers.add(DisplayNavigationServiceHandler.getInstance());
        this.serviceHandlers.add(new TelephonyServiceHandler(this));
        this.serviceHandlers.add(new SpeechServiceHandler());
        this.serviceHandlers.add(VoiceServiceHandler.getInstance());
        this.serviceHandlers.add(MusicServiceHandler.getInstance());
        this.serviceHandlers.add(contactServiceHandler);
        this.serviceHandlers.add(new PhotoServiceHandler(this));
        this.serviceHandlers.add(DestinationsServiceHandler.getInstance());
        this.serviceHandlers.add(new MessageServiceHandler());
        this.serviceHandlers.add(new SettingsServiceHandler());
        this.serviceHandlers.add(TripUpdateServiceHandler.getInstance());
        this.serviceHandlers.add(new PlaceTypeSearchServiceHandler());
        if (BaseActivity.weHaveContactsPermission()) {
            getContentResolver().registerContentObserver(Contacts.CONTENT_URI, true, new ContactObserver(contactServiceHandler, this.serviceHandler));
        }
    }

    protected ConnectionListener[] getConnectionListeners(Context context) {
        return new ConnectionListener[]{new AcceptorListener(context, new BTSocketAcceptor("Navdy", NAVDY_PROTO_SERVICE_UUID), ConnectionType.BT_PROTOBUF)};
    }

    protected RemoteDeviceBroadcaster[] getRemoteDeviceBroadcasters() {
        return new RemoteDeviceBroadcaster[0];
    }

    protected ProxyService createProxyService() throws IOException {
        return new ProxyListener(new BTSocketAcceptor("Navdy-Proxy-Tunnel", NAVDY_PROXY_TUNNEL_UUID));
    }

    public RemoteDevice getRemoteDevice() {
        return this.mRemoteDevice;
    }

    public IBinder onBind(Intent intent) {
        if (!getClass().getName().equals(intent.getAction())) {
            return super.onBind(intent);
        }
        this.logger.d("returning local binder");
        return this.binder;
    }

    protected void setRemoteDevice(RemoteDevice remoteDevice) {
        boolean deviceChanged = this.mRemoteDevice != remoteDevice;
        super.setRemoteDevice(remoteDevice);
        if (deviceChanged) {
            this.deviceChangeBroadcaster.dispatchDeviceChangedEvent(remoteDevice);
        }
    }
}
