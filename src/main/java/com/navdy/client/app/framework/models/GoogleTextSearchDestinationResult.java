package com.navdy.client.app.framework.models;

import android.support.annotation.NonNull;
import com.amazonaws.services.s3.internal.Constants;
import com.navdy.client.app.framework.i18n.AddressUtils;
import com.navdy.client.app.framework.models.Destination.SearchType;
import com.navdy.client.app.framework.models.Destination.Type;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.providers.NavdyContentProviderConstants;
import com.navdy.service.library.log.Logger;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.droidparts.contract.SQL.DDL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GoogleTextSearchDestinationResult {
    public static final String FORMATTED_PHONE_NUMBER = "formatted_phone_number";
    public static final String INTERNATIONAL_PHONE_NUMBER = "formatted_phone_number";
    static final Logger logger = new Logger(GoogleTextSearchDestinationResult.class);
    public String address;
    public String city;
    public String country;
    public String countryCode;
    public double distance;
    public String icon;
    public String jsonString;
    public String lat;
    public String lng;
    public String name;
    public List<String> open_hours;
    public boolean open_now;
    public String phone;
    public String place_id;
    public String price_level;
    public String rating;
    public String state;
    public String streetName;
    public String streetNumber;
    public String[] types;
    public String url;
    public String zipCode;

    public static GoogleTextSearchDestinationResult createDestinationObject(String jsonString) {
        try {
            return new GoogleTextSearchDestinationResult(new JSONObject(jsonString));
        } catch (JSONException e) {
            logger.e("Unable to parse the json string for a Google text search result: " + jsonString, e);
            return null;
        }
    }

    public static GoogleTextSearchDestinationResult createDestinationObject(JSONObject jsonObject) {
        try {
            return new GoogleTextSearchDestinationResult(jsonObject);
        } catch (JSONException e) {
            logger.e("Unable to parse the json object for a Google text search result: " + jsonObject, e);
            return null;
        }
    }

    private GoogleTextSearchDestinationResult(JSONObject jsonObject) throws JSONException {
        int i;
        this.jsonString = jsonObject.toString();
        if (jsonObject.has("name")) {
            this.name = jsonObject.getString("name");
        } else if (jsonObject.has("terms")) {
            JSONArray terms = jsonObject.getJSONArray("terms");
            this.name = terms.getJSONObject(0).getString("value");
            ArrayList<String> addressParts = new ArrayList<>();
            for (i = 1; i < terms.length(); i++) {
                addressParts.add(terms.getJSONObject(i).getString("value"));
            }
            this.address = StringUtils.join(addressParts, DDL.SEPARATOR);
        } else if (jsonObject.has("description")) {
            this.name = jsonObject.getString("description");
        }
        if (jsonObject.has("geometry")) {
            JSONObject location = jsonObject.getJSONObject("geometry").getJSONObject("location");
            this.lat = location.getString("lat");
            this.lng = location.getString("lng");
        }
        if (jsonObject.has("routes")) {
            JSONArray results = (JSONArray) jsonObject.get("routes");
            logger.d("Routes: " + (results != null ? results.toString() : Constants.NULL_VERSION_ID));
            if (results != null && results.length() > 0) {
                JSONObject firstRoute = results.getJSONObject(0);
                if (firstRoute != null) {
                    JSONArray legs = (JSONArray) firstRoute.get("legs");
                    if (legs != null && legs.length() > 0) {
                        JSONObject lastLeg = legs.getJSONObject(legs.length() - 1);
                        if (lastLeg != null) {
                            JSONObject endLocation = lastLeg.getJSONObject("end_location");
                            if (endLocation != null) {
                                String endLocationLng = endLocation.getString("lng");
                                String endLocationLat = endLocation.getString("lat");
                                logger.d("Received endLocation from the last leg: " + endLocationLat + DDL.SEPARATOR + endLocationLng);
                                if (!(StringUtils.isEmptyAfterTrim(endLocationLat) || StringUtils.isEmptyAfterTrim(endLocationLng))) {
                                    this.lat = endLocationLat;
                                    this.lng = endLocationLng;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (jsonObject.has("lat") && jsonObject.has("lng")) {
            this.lat = jsonObject.getString("lat");
            this.lng = jsonObject.getString("lng");
        }
        if (jsonObject.has("opening_hours")) {
            this.open_hours = new ArrayList<>();
            JSONObject opening_hours = jsonObject.getJSONObject("opening_hours");
            this.open_now = Boolean.valueOf(opening_hours.getString("open_now"));
            if (jsonObject.has("weekday_text")) {
                JSONArray weekday_text = opening_hours.getJSONArray("weekday_text");
                if (weekday_text.length() > 0) {
                    for (i = 0; i < weekday_text.length(); i++) {
                        String dayOfWeek = weekday_text.getString(i);
                        if (dayOfWeek != null) {
                            this.open_hours.add(dayOfWeek);
                            logger.v("day of week: " + dayOfWeek);
                        }
                    }
                }
            }
        }
        if (jsonObject.has("formatted_address")) {
            this.address = jsonObject.getString("formatted_address");
        } else if (jsonObject.has("vicinity")) {
            this.address = jsonObject.getString("vicinity");
        }
        if (AddressUtils.isTitleIsInTheAddress(this.name, this.address)) {
            this.name = "";
        }
        JSONArray addressComponents = jsonObject.optJSONArray("address_components");
        if (addressComponents != null) {
            for (int aci = 0; aci < addressComponents.length(); aci++) {
                try {
                    JSONObject obj = addressComponents.getJSONObject(aci);
                    String longName = obj.getString("long_name");
                    JSONArray types = obj.getJSONArray("types");
                    int ti = 0;
                    while (ti < types.length()) {
                        String type = types.getString(ti);
                        if (NavdyContentProviderConstants.DESTINATIONS_STREET_NUMBER.equals(type)) {
                            this.streetNumber = longName;
                            break;
                        } else if ("route".equals(type)) {
                            this.streetName = longName;
                            break;
                        } else if ("locality".equals(type)) {
                            this.city = longName;
                            break;
                        } else if ("administrative_area_level_1".equals(type)) {
                            this.state = longName;
                            break;
                        } else if (NavdyContentProviderConstants.DESTINATIONS_COUNTRY.equals(type)) {
                            this.country = longName;
                            this.countryCode = obj.getString("short_name");
                            break;
                        } else if ("postal_code".equals(type)) {
                            this.zipCode = longName;
                            break;
                        } else {
                            ti++;
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
        if (jsonObject.has("formatted_phone_number")) {
            this.phone = jsonObject.getString("formatted_phone_number");
        }
        if (jsonObject.has("website")) {
            this.url = jsonObject.getString("website");
        }
        if (jsonObject.has(SettingsJsonConstants.APP_ICON_KEY)) {
            this.icon = jsonObject.getString(SettingsJsonConstants.APP_ICON_KEY);
        }
        if (jsonObject.has(NavdyContentProviderConstants.DESTINATIONS_PLACE_ID)) {
            this.place_id = jsonObject.getString(NavdyContentProviderConstants.DESTINATIONS_PLACE_ID);
        }
        if (jsonObject.has("types")) {
            this.types = jsonObject.getJSONArray("types").toString().split(DDL.SEPARATOR);
        }
        if (jsonObject.has("price_level")) {
            this.price_level = jsonObject.getString("price_level");
        }
        if (jsonObject.has("rating")) {
            this.rating = jsonObject.getString("rating");
        }
        jsonObject.remove("reviews");
        jsonObject.remove("photos");
        jsonObject.remove("reference");
        this.jsonString = jsonObject.toString();
    }

    public String toString() {
        return String.format("Name: %s,\tLat: %s,\tLng: %s,\tAddress: %s,\tstreetNumber: %s,\tstreetName: %s,\tcity: %s,\tstate: %s,\tzipCode: %s,\tcountry: %s (%s),\tSuggestionType: %s,\tRating: %s,\tPrice Level: %s,\tPlace Id: %s,\tOpen Hours: %s,\tOpen Now: %s,\tPhone: %s,\tUrl: %s", new Object[]{this.name, this.lat, this.lng, this.address, this.streetNumber, this.streetName, this.city, this.state, this.zipCode, this.country, this.countryCode, Arrays.toString(this.types), this.rating, this.price_level, this.place_id, this.open_hours, Boolean.valueOf(this.open_now), this.phone, this.url});
    }

    public void toModelDestinationObject(SearchType searchResultType, Destination destination) {
        destination.name = this.name;
        double newDisplayLat = Double.parseDouble(this.lat);
        double newDisplayLong = Double.parseDouble(this.lng);
        if (!(newDisplayLat == destination.displayLat && newDisplayLong == destination.displayLong)) {
            destination.navigationLat = 0.0d;
            destination.navigationLong = 0.0d;
        }
        destination.displayLat = newDisplayLat;
        destination.displayLong = newDisplayLong;
        destination.rawAddressNotForDisplay = this.address;
        destination.streetNumber = this.streetNumber;
        destination.streetName = this.streetName;
        destination.city = this.city;
        destination.state = this.state;
        destination.zipCode = this.zipCode;
        destination.country = this.country;
        destination.setCountryCode(this.countryCode);
        destination.placeId = this.place_id;
        destination.searchResultType = searchResultType.getValue();
        destination.placeDetailJson = this.jsonString;
        destination.type = getType(this.types);
    }

    private Type getType(String[] types) {
        Type destinationType = Type.UNKNOWN;
        if (types == null || types.length <= 0) {
            return destinationType;
        }
        try {
            return parseTypes(new JSONArray(types[0]));
        } catch (JSONException e) {
            e.printStackTrace();
            return destinationType;
        }
    }

    public static Type parseTypes(JSONArray array) throws JSONException {
        Type destinationType = Type.UNKNOWN;
        if (array.length() <= 0) {
            return destinationType;
        }
        for (int i = 0; i < array.length(); i++) {
            Type tempType = getType((String) array.get(i));
            if (tempType != Type.UNKNOWN && tempType != Type.PLACE) {
                return tempType;
            }
            if (destinationType == Type.UNKNOWN) {
                destinationType = tempType;
            }
        }
        return destinationType;
    }

    @NonNull
    private static Type getType(@NonNull String type) {
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "accounting")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "airport")) {
            return Type.AIRPORT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "amusement_park")) {
            return Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "aquarium")) {
            return Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "art_gallery")) {
            return Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "atm")) {
            return Type.ATM;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bakery")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bank")) {
            return Type.BANK;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bar")) {
            return Type.BAR;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "beauty_salon")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bicycle_store")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "book_store")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bowling_alley")) {
            return Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bus_station")) {
            return Type.TRANSIT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "cafe")) {
            return Type.COFFEE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "campground")) {
            return Type.PARK;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "car_dealer")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "car_rental")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "car_repair")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "car_wash")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "casino")) {
            return Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "cemetery")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "church")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "city_hall")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "clothing_store")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "convenience_store")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "courthouse")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "dentist")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "department_store")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "doctor")) {
            return Type.HOSPITAL;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "electrician")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "electronics_store")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "embassy")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "establishment")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "finance")) {
            return Type.BANK;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "fire_station")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "florist")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "food")) {
            return Type.RESTAURANT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "funeral_home")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "furniture_store")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "gas_station")) {
            return Type.GAS_STATION;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "general_contractor")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "grocery_or_supermarket")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "gym")) {
            return Type.GYM;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "hair_care")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "hardware_store")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "health")) {
            return Type.HOSPITAL;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "hindu_temple")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "home_goods_store")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "hospital")) {
            return Type.HOSPITAL;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "insurance_agency")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "jewelry_store")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "laundry")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "lawyer")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "library")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "liquor_store")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "local_government_office")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "locksmith")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "lodging")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "meal_delivery")) {
            return Type.RESTAURANT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "meal_takeaway")) {
            return Type.RESTAURANT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "mosque")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "movie_rental")) {
            return Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "movie_theater")) {
            return Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "moving_company")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "museum")) {
            return Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "night_club")) {
            return Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "painter")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "park")) {
            return Type.PARK;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "parking")) {
            return Type.PARKING;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "pet_store")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "pharmacy")) {
            return Type.HOSPITAL;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "physiotherapist")) {
            return Type.HOSPITAL;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "place_of_worship")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "plumber")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "police")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "post_office")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "real_estate_agency")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "restaurant")) {
            return Type.RESTAURANT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "roofing_contractor")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "rv_park")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "school")) {
            return Type.SCHOOL;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "shoe_store")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "shopping_mall")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "spa")) {
            return Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "stadium")) {
            return Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "street_address")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "storage")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "store")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "subway_station")) {
            return Type.TRANSIT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "synagogue")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "taxi_stand")) {
            return Type.TRANSIT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "train_station")) {
            return Type.TRANSIT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "transit_station")) {
            return Type.TRANSIT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "travel_agency")) {
            return Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "university")) {
            return Type.SCHOOL;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "veterinary_care")) {
            return Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "zoo")) {
            return Type.ENTERTAINMENT;
        }
        return Type.UNKNOWN;
    }
}
